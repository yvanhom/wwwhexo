---
uuid:             bb0ee682-116b-4e11-8665-8f0bc8c596af
layout:           post
title:            '使用 Bluemix 之 Docker'
slug:             bluemix-docker-container
subtitle:         null
date:             '2016-12-13T01:22:01.000Z'
updated:          '2016-12-13T06:39:55.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

简单记录下一些关键的步骤，给自己看。

### 从 Github 中下载并安装 cf

```
https://github.com/cloudfoundry/cli/releases
```

### 下载并安装 ibm 的插件

```
wget https://static-ice.ng.bluemix.net/ibm-containers-linux_x64
cf install-plugin ibm-containers-linux_x64
```

### cf 登录

```
cf login
# API endpoint> https://api.ng.bluemix.net
# Email> example@example.org
# Password> example

cf ic login
```

### 从 docker hub 复制镜像到 bluemix

```
cf ic cpi xxxx/xxxx registry.ng.bluemix.net/xxx/xxx
```

### 从 Bluemix 的网站中新建 docker

* 选择复制的镜像
* 选择内存需求
* 设置开放端口
* 设置好环境变量
* 创建即可

### 例子：Shadowsocks

翻墙用。

**从 docker hub 复制镜像到 bluemix**

```
cf ic cpi vimagick/shadowsocks-libev registry.ng.bluemix.net/yvanhom/my-ssev
```

**设置**

* 内存只需要 64M
* 开放端口 9080
* 增加环境变量
 * SERVER_PORT 9080
 * PASSWORD xxxxxxxx
 * METHOD aes-256-cfb （可选，默认即可）
 * TIMEOUT 300（可选，默认即可）

### 例子：cloud-torrent

离线种子下载。

**从 docker hub 复制镜像到 bluemix**

```
cf ic cpi jpillora/cloud-torrent registry.ng.bluemix.net/yvanhom/my-cloudtorrent
```

**设置**

* 内存选择 256M
* 开放端口 8080 和 9080
* 增加环境变量
 * PORT 9080

**访问**

* 从 xxx.xxx.xxx.xxx:9080 访问。

### 注意

* Bluemix 的 docker 容器存储容量 10G。
* Bluemix 的免费配额是一个月 365G 小时，即一个月最多 512M 不下线，所以所建容器的内存配额尽量小。
* Bluemix 的公共 IP 有两个，所以一般手段只能使用两个容器，或许建个公共的 port forward，再连接多个内部 IP，没准可行。
* Bluemix 的入站和出站没有流量总额限制，赞。
