---
uuid:             3a95dc24-9f03-41e8-b0b8-b9faef95fed5
layout:           post
title:            使用lua来简单分析日志
slug:             lua-analysis-log
subtitle:         null
date:             '2014-09-14T22:14:27.000Z'
updated:          '2015-07-29T22:44:41.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---



## 1, 使用lua分析日志的好处

使用lua来分析日志，好处是灵活，配合lua的字符串库，可以很简单的做到匹配，数据提取，计算，分析等。

对比shell的grep，awk等，可以处理更复杂更多样的文本格式，直观的多样化输出等等。


## 2, 情况分析

遇到的情形是这样的，两个节点，在发送数据包时，发现时不时会有卡住现象。

这里使用lua分析日志，判断是否有卡住现象发生，发生的频率，卡住的持续时间等。

日记格式如下：

```
...
local : null
remote : null
14/09/13 01:37:39 INFO mctcp.TestMCNetBlock: Receive Packet 0
14/09/13 01:37:39 INFO mctcp.TestMCNetBlock: Receive Packet 1
...
```

某节点发起连接，会输出一些提示信息，这些对我们没用，然后开始发送数据包，  
 发送节点会记录发送的时间，接收节点会记录接收的时间。

所以现在的任务是解析出每个连接内，数据包发送/接受的间隔时间，如果这个间隔时间太长，  
 则认为这里卡住。观察日志发现，正常的数据包发送/接收时间都不超过1s。


## 3, 实现说明

逐行分析日志：

```
for line in io.lines() do
  -- 分析日志
done
```

首先需要分离每个连接，当日志遇到非时间行时，认为连接断开/建立新连接:

```
   -- 解析出时间
   hour,minute,second = string.match(line, "(%d+):(%d+):(%d+)")
   if hour == nil then
      -- 非时间行，认为连接断开
      isend = true
   else
      -- 时间行，进行处理
      isend = false
   end
```

当遇到时间行时，如果这是新连接，则不输出，否则输出包间隔时间，如果时间间隔为1s，也当作0s处理：

```
      -- 计算时间
      currenttime = hour * 3600 + minute * 60 + second
      cost = currenttime - lasttime
      -- 时间间隔为1s，当作0s处理
      if cost == 1 then
         cost = 0
      end
      if isend == false then
         -- 非新连接，输出包间隔时间
         print(cost)
      end
      lasttime = currenttime
```

所有的代码如下：

```
lasttime = 0
currenttime = 0
isend = false
 
for line in io.lines() do
--   print(line)
   hour,minute,second = string.match(line, "(%d+):(%d+):(%d+)")
   if hour == nil then
      isend = true
   else
      currenttime = hour * 3600 + minute * 60 + second
      cost = currenttime - lasttime
      if cost == 1 then
         cost = 0
      end
      if isend == false then
         print(cost)
      end
      lasttime = currenttime
      isend = false
   end
--   print(hour,minute,second)
end
```



