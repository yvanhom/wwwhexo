---
uuid:             41b2a1c9-69af-4264-8482-a1ad17abd736
layout:           post
title:            'MSYS2 + ConEmu'
slug:             msys2-conemu
subtitle:         null
date:             '2015-11-03T22:11:18.000Z'
updated:          '2015-12-30T20:22:42.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---

喜欢 Linux/Unix，喜欢使用 Linux 下的命令，包括 vim，awk，grep，shell，lua 等，于是在 Windows 下也安装了 Cygwin，后来又改用 MSYS2。

相对 Cygwin，更喜欢 MSYS2，因为 pacman，包管理比 Cygwin 更容易多，不过 Cygwin 更强大，连 X 环境都支持了，可惜我用得少，唯一的用到的便是 ssh 的 X 转发。

无论是 Cygwin，还是 MSYS2，最困扰的还是终端 Terminal，自带的 MinTTY 虽然强大，但是不支持多标签，没有 Multiple Tabs，使用起来很不方便，看着一大堆窗口，真是头疼。

有一些工具很有用：

* [ConEmu](https://github.com/Maximus5/ConEmu) 
* [cmder](http://cmder.net/)
* [ConsoleZ](https://github.com/cbucher/console/)

ConsoleZ，貌似排版有问题，cmder 封装了 ConEmu，所以最后我选择了 ConEmu。

使用 ConEmu，存在几个很影响体验的问题：

1. 高亮，ssh 后运行的命令高亮部分总是显示为灰色背景黑色字体，影响体验
2. 部分命令运行时输出重叠，比如 vim 命令运行时不清空屏幕，运行后不清空屏幕，于是显示重叠，妨碍使用，完全无法忍受。
3. 使用 ssh 不更改标签标题，当使用很多个 ssh 命令后，多个标签难以找到想要的那个，只能一个个点进去查看内容，麻烦

一番折腾，发现这三个问题是一个问题，那就是 TERM 环境变量，在 /usr/share/terminfo 中有很多个可选择项，尝试了很多个，带 cygwin 的不能解决问题 3, 带 xterm 的能解决问题 3,但总是有问题 2, 最后发现 xterm-color 能解决上述三个问题，便不再折腾了。

这里将整个流程叙述一遍：

* 安装 MSYS2，ConEmu
* ConEmu -- Settings -- Startup --Tasks 中增加 msys2:bash，使用命令

``` sh
E:\Tools\msys64\usr\bin\bash.exe --login -i
```

* ConEmu -- Settings -- Startup 中 Specified named task 中选择 {msys2:bash}
* 进入 ConEmu 的 msys2:bash，此时会进入 ~ 目录，编辑 .bashrc 加入

``` sh
export TERM=xterm-color
```

**2015/11/05 补充**

使用 screen 时，问题不少，在 .bashrc 上增加下述代码，将 screen 的 TERM 设置为 screen2，效果可以好一点。

``` sh
if [[ $TERM = screen ]]; then
        export TERM=screen2  
fi                           
```

不过问题仍然存在，有时切出 screen，或者退出 ssh，经常会不清空终端，造成文字显示覆盖。

麻烦啊，还是 Linux 下的 Terminal 好用。

**2015/11/05 再补充**

崩溃，问题 2 非常严重，已经影响到使用了。

唉，放弃了。

使用 cygwin，虽然有问题 3，但是相比起有问题 2，好多了。

``` sh
export TERM=cygwin
```

**2015/12/24 补充**

发现 ConEmu 开发者们提供了 [Cygwin/MSYS connector](https://github.com/Maximus5/cygwin-connector)，于是迫不及待地尝试，看看之前的那些问题有没有完美地解决。

首先把 msys2，ConEmu 等都升级，按照[文档](https://conemu.github.io/en/CygwinMsysConnector.html)把 Cygwin/MSYS connector 安装，在 ConEmu 中设置使用。

安装 Cygwin/MSYS connector 很简单，把下载下来的 conemu-msys2-64.exe 放置到 msys2 的可执行目录 /usr/bin 下。

在 ConEmu 中的设置也很容易，就是增加一个 task，并设置执行命令为 conemu-msys2-64.exe 的路径。

目前看来，没有上述的那些问题，都完美解决了。


**2015/12/24 再补充**

使用 Cygwin/MSYS connector，还是有问题，目前遇到的问题是有时候上下左右键无效果，即是不能使用上下翻历史记录，使用左右移动光标。

**2015/12/32 补充**

放弃 Cygwin/MSYS connector，返回使用原来的设置 TERM 的方式。唉，细节决定成败，这个细节问题真是影响心情。
