---
uuid:             876364c4-3206-47ae-8d3e-2fc753bb511c
layout:           post
title:            '使用curl调用Rest API'
slug:             curl-restapi
subtitle:         null
date:             '2014-12-30T21:20:06.000Z'
updated:          '2015-07-29T22:40:49.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---


最近在使用FloodLight，接触了不少Rest API的使用，参照官网所说，使用curl来简单调用，查看需要的信息。

下边是我整理的脚本，使用curl调用Rest API，并用python美化输出结果：

```
#!/bin/bash
CONTROLLER=192.168.56.1
 
if [ "$2" != "" ]; then
        ARGS="-d ${2}"
fi
 
if [ "$3" != "" ]; then
        ARGS="$ARGS -X $3"
fi
 
#echo $ARGS
 
# curl使用-s关闭统计信息输出，使用python美化json输出
#curl -s $ARGS http://${CONTROLLER}:8080/${1}
curl -s $ARGS http://${CONTROLLER}:8080/${1} | python -m json.tool
 
echo
```

使用格式如下：

    ./rest.sh RestURL [json参数] [GET/DELETE]

例子如下：

#### 例子1：

    ./rest.sh wm/core/memory/json

shell解析成：

    curl -s http://192.168.56.1:8080/wm/core/memory/json | python -m json.tool

输出：

```
{
    "free": 208824192,
    "total": 334495744
}
```

####  例子2：带参数

    ./rest.sh wm/staticflowentrypusher/json '{"switch":"00:00:00:00:00:00:00:01","name":"flow-mod-1","priority":"32768","ingress-port":"1","active":"true","actions":"output=2"}'

输出：

```
{
    "status": "Entry pushed"
}
```

#### 例子3：使用DELETE

    ./rest.sh wm/staticflowentrypusher/json '{"name":"flow-mod-1"}' DELETE

输出：

```
{
    "status": "Entry flow-mod-1 deleted"
}
```



