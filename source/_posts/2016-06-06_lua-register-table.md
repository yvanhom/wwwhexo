---
uuid:             0d55f15e-5782-4417-8f71-56b1f8841627
layout:           post
title:            'Lua 注册表'
slug:             lua-register-table
subtitle:         null
date:             '2016-06-06T05:51:01.000Z'
updated:          '2016-06-06T05:52:11.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---

Lua 注册表的数据是 lua C 环境下能全局访问，而 lua 环境无法直接访问的数据。我们知道，lua 每调用一次 C 函数，都会重新生成堆栈，使用堆栈在两次调用间保存数据，这不可能，要在两次调用间使用同一分数据，要么做好参数传递，要么使用全局变量，要么使用注册表，第一种麻烦，第二种不安全，第三种比较合适，因为不会被 lua 语句直接访问数据。

lua 的注册表主要用在回调函数，回调函数如何传递变量，是一个麻烦的问题。

lua 注册表使用 LUA_REGISTRYINDEX 做为伪索引对 lua_State 上的堆栈进行索引，然后像 table 一样使用注册表。

设置和获取是最直观的操作：

```c
// 设置
lua_pushstring(L, "apple");
lua_setfield(L, LUA_REGISTRYINDEX, "key");

// 获取
lua_getfield(L, LUA_REGISTRYINDEX, "key");
const char *s = lua_checkstring(L, -1);
```

当然，还有几个比较复杂的函数：

```c
// 如果注册表已经有了 name，那么返回了
// 否则创建一个新表，放入注册表，key 为 name
// 完全不涉及到 lua_State 的堆栈
luaL_newmetatable(L, name);

// 获取注册表 name 处的值，将它设置为堆栈顶部元素的 metatable
// 不改变堆栈
luaL_setmetatable(L, name);

// 获取注册表中 name 的值
luaL_getmetatable(L, name);
```

上边的几个函数跟下边的几个函数很像，仅仅相差一个 L，只是下边的函数不涉及注册表：

```c
// 获取第 n 个位置的表的 metatable，压入栈中
lua_getmetatable(L, n);

// 将栈顶元素设置为第 n 处元素的 metatable，栈顶元素弹出
lua_setmetatable(L, n);

```
