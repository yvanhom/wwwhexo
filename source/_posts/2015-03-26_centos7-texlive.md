---
uuid:             9f2dd2ea-0f3d-414f-b241-7004f4ffc645
layout:           post
title:            'CentOS 7下在线安装TeX Live'
slug:             centos7-texlive
subtitle:         null
date:             '2015-03-26T07:09:21.000Z'
updated:          '2015-07-29T22:36:43.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


CentOS 7的yum库中的TeX Live版本比较老，是2012年的版本，所以直接安装官网上的版本。

### 1, 下载在线安装程序

在[官网页面](http://tug.org/texlive/acquire-netinstall.html "TeX Live官网下载页")下载安装包[install-tl-unx.tar.gz](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz "TeX Live在线安装包")。

### 2, 运行安装

解压安装包，运行安装：

`./install-tl`

结果报错：

```
Can't locate Digest/MD5.pm in @INC (@INC contains: ./tlpkg /usr/local/lib64/perl5 /usr/local/share/perl5 /usr/lib64/perl5/vendor_perl /usr/share/perl5/vendor_perl /usr/lib64/perl5 /usr/share/perl5 .) at tlpkg/TeXLive/TLUtils.pm line 204.
BEGIN failed--compilation aborted at tlpkg/TeXLive/TLUtils.pm line 204.
Compilation failed in require at ./install-tl line 53.
BEGIN failed--compilation aborted at ./install-tl line 53.
```

perl缺少一些依赖，使用yum查找相关包：

`yum search perl | grep -i md5`

找到perl-Digest-MD5.x86_64，安装后再重新运行：

```
yum install perl-Digest-MD5
./install-tl
```

貌似会自动查找较近的镜像站，我运行时使用的是教育网的镜像，速度不错。

安装过程中提示：

```
Actions:
 <I> start installation to hard disk
 <H> help
 <Q> quit
 
Enter command: 
```

输入**I**开始安装。

### 安装后设置

在～/.bashrc中设置路径PATH：

`export PATH=$PATH:/usr/local/texlive/2014/bin/x86_64-linux`

到此Tex Live便安装完成，可以说非常简单，没有遇到多大的困难。



