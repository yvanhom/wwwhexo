---
uuid:             a74d47fc-6605-49b5-a442-3ea46a3c8b7f
layout:           post
title:            'OpenVSwitch 通过隧道连接两台物理机上的虚拟机'
slug:             openvswitch-connect-two-physical-machine
subtitle:         null
date:             '2015-04-17T02:40:51.000Z'
updated:          '2015-07-29T22:35:45.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - sdx

---


在 OpenVSwitch 上，构建虚拟机集群，往往需要使用多台物理机，而物理机之间使用的是 L3 协议链接，而 OpenVSwitch 的虚拟交换机是位于 L2 层，于是在做 OpenVSwitch 上的跨物理机的虚拟交换机连接时，需要使用隧道，可选择 GRE 或者 VXLAN。

具体的配置方法参考[这里](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_OpenStack_Platform/4/html/Installation_and_Configuration_Guide/Configuring_Open_vSwitch_tunnels.html "Configure Open vSwitch Tunnels")。

在多个网站上都说到，使用 GRE 隧道会造成性能的损失。

而我在使用时，在上边部署Hadoop平台，总是有异常，最常见的便是 CHECKSUM error，校验失败，表示传输时数据出错。

刚开始以为是 hadoop 使用的是不同时期的编译二进制文件，openjdk 的版本不一致，OpenVSwitch 的版本不一致。

可是把这些都整一致后，还是有问题，只能怀疑是 GRE 隧道有问题。

怎么办？

查看[文档](http://openvswitch.org/ovs-vswitchd.conf.db.5.pdf)发现 OpenVSwitch 的 GRE 隧道有一个 csum 的设置，可以强制增加校验，虽然会导致性能的再一次下降。

于是增加隧道端口的命令变成

```
sudo ovs-vsctl add-port br0 gre0 -- set Interface gre0 type=gre options:remote_ip=192.168.3.178 options:csum=true
```

终于，烦人的 CHECKSUM error 不见了。

至于性能，iperf 的带宽测试，1Gbps 的网络能够达到 850 Mbits/s，还算可以。



