---
uuid:             64cace45-c2c1-4704-ae9e-25ee6a0866c0
layout:           post
title:            'shell编程实例：iostat + read + Rest API'
slug:             shell-iostat-read-rest-api
subtitle:         null
date:             '2015-03-18T08:34:27.000Z'
updated:          '2015-07-29T22:37:44.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---


### 场景

节点上运行iostat，每隔一段时间收集磁盘IO信息，解析，提取，然后将提取的信息通过Rest API传给某个应用使用。

### 代码与分析

代码如下：

```
#!/bin/bash
 
IP=10.0.0.1
 
# 使用iostat每隔5秒获取/dev/sda的IO信息，-x表示输出详细信息，-d表示只输出磁盘信息，默认还输出CPU和内存信息
iostat -x -d /dev/sda 5 | while read line; do
        # iostat的输出，有列描述信息行，也有IO信息行，这里过滤掉列描述信息行
        if [[ ${line} == sda* ]]; then
                # 取出最后一个%util参数，这里对line截掉从左开始最长匹配'sda* '的字符串
                diskutil=${line##sda* }
                # 构造json参数
                args="{"host":"$IP","diskutil":$diskutil}"
                # echo $args
                # 使用curl调用Rest API，最后使用python -m json.tool美化输出
                curl  -d "$args" https://${IP}:8080/wm/ns/hoststat/json  | python -m json.tool
        fi
done
```

### 其它

本来想使用grep做行的过滤，awk做列的提取，结果遇到了问题，输出结果被缓存起来，不再是行缓存，而是全缓存，于是不能达到每5秒发送统计信息的效果。

其次，curl的输出结果是没有换行的JSON字符串，要直观的话，可以使用python -m json.tool进行格式化。



