---
uuid:             67db05b3-f96e-466f-96e3-f953051304fb
layout:           post
title:            神奇的JGroups异常JGRP000032
slug:             jgroups-exception
subtitle:         null
date:             '2014-08-11T01:00:01.000Z'
updated:          '2015-07-29T22:44:52.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - java

---


最近使用了下JGroups，遇到了一个很神奇的异常：

某节点：

    JGRP000032: XXX: no physical address for XXX, dropping message

另一个节点则显示接收到非组内成员的message，同样是dropping message。

说这个异常神奇，是因为他的触发很奇怪。

先说下我使用的两台机子，运行centos5，内核2.6.18，在同一局域网内，有ipv4，可以连接外网，启动ipv6，不过ipv6没用。使用的JGroups是最新版本3.6.0。为了方便，把防火墙给关了（iptables -F）。测试以下两种情况：

1. 同一台机子上的两个jgroups成语能否通信
2. 不同机子上的两个jgroups成员能否通信

测试后的结果如下：

- 使用JGroups自带的默认配置，JGroups会绑定ipv6地址(bind_addr)，结果是不同机子上的程序能够互相通信，同一台机子上的却不能。
- 使用JGroups自带的默认配置，设置*java.net.prefer*IP4Stack=true（System.setProperty(“*java.net.prefer*IP4Stack”, “true”）;)，上述两种情况都能正常运行。
- 使用JGroups自带的默认配置，设置jgroups.bind_addr=hostname，绑定在ipv4的网络接口上，最后机子A上的a1创建了组播组，然后A上的a2加入成功，可是在机子B上的b1加入后，a2却退出了。
- 此外在运行的时候，有时会出现a1创建组播组，b1加入后看不到a1的情况。

面对以上这些情况，对于不想深入了解JGroups那么多层协议栈，只是想使用JGroups的udp组播+Java+可靠链接的我，实在是无能为力，不过还有上边的第二项配置能够正常使用，勉强凑合着吧。

JGroups的建组/加组真心慢，足足花费了2~3s，真心伤不起。

 



