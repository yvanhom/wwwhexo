---
uuid:             c7234c73-102f-4444-bea1-1912c9bd4d19
layout:           post
title:            FreeBSD字体锯齿效果严重
slug:             freebsd-fonts
subtitle:         null
date:             '2015-01-12T21:15:39.000Z'
updated:          '2015-07-29T22:39:46.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---


用了几个月的FreeBSD，一直为xfce字体的锯齿效果所困扰，字显示得非常难看，期间换了多个桌面环境，下载了无数其它字体，比如最近的Source Hans Sans，还是没法解决问题。

不过一开始有Unibit这个神奇的字体，在别的字体都各种锯齿，缺横短撇，模模糊糊的时候，这个字体却显示得很平滑，很美观。

不过后来不知道更新了什么，这个字体不见了。

于是安装了很多其它字体，不断选择，最后只有文泉驿点阵正黑效果还稍微好一点。

没想到今天恰好看到[这篇文章](https://www.librehat.com/solve-freebsd-dragonflybsd-font-antialiasing-settings-invalid-issues/ "FreeBSD字体抗锯齿")，马上尝试一下，结果果然有效，文件一修改，xfce的字体立马像复活了一般，修改成别的字体也都很美观。

之前抗锯齿为什么没起效，原因很简单，wqy-fonts在/usr/local/etc/fonts/conf.d/85-wqy.conf中设置了，当字体大小小于等于16时，将字体抗锯齿关掉。这是全局的设置，不止针对文泉驿字体，这个设置真是太霸道了，便是下边这两句：

```
                <test name="size" compare="less_eq"><int>16</int></test>
                <edit name="antialias" mode="assign"><bool>false</bool></edit>
```

毫不犹豫地，把上边的16改小：

```
                <test name="size" compare="less_eq"><int>9</int></test>
                <edit name="antialias" mode="assign"><bool>false</bool></edit>
```

改成多少无所谓，我现在xfce用的字体都是10，12，14，所以就改成9了。

一经修改，立马生效，不能再赞了！



