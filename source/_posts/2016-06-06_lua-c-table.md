---
uuid:             c11b7ada-38cf-42f1-9495-ed23be04f9d6
layout:           post
title:            'lua 表的 C 语言操作'
slug:             lua-c-table
subtitle:         null
date:             '2016-06-06T07:29:50.000Z'
updated:          '2016-06-06T07:30:58.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---

创建表：

```c
// 创建表，压人栈中
lua_newtable(L);
```

元素访问：
```c
// 表在 index 处，key 为栈顶元素，or 整数 i，or 字符串 name
// key 为栈顶元素时，出栈
// value 压栈
lua_gettable(L, index); 
lua_geti(L, index, i);
lua_getfield(L, n, name);

// 表在 index 处，key 为栈中位置 -2 的元素，or 整数 i，or 字符串 name, value 为栈顶元素
// value 出栈
// key 为栈中位置 -2 的元素时，出栈
lua_settable(L, index);
lua_seti(L, index, i);
lua_setfield(L, index, name);
```

元素访问（raw）
```
lua_rawget(L, index);
lua_rawgeti(L, index, i);
lua_rawgetp(L, index, name);

lua_rawset(L, index);
lua_rawseti(L, index, i);
lua_rawsetp(L, index, name);
```

元表操作：
```
// 获取 index 处的表的元表，入栈
lua_getmetatable(L, index);
// 将栈顶元素设置为 index 处表的元表，出栈
lua_setmetatable(L, index);
```

迭代访问：
```
// 迭代反问 index 处的表，还有数据时返回 1，否则返回 0
// 以栈顶元素作为上一个 key，弹出，无论是否有数据，总是弹出
// 有数据时，压入“key”，“value”
lua_next(L, index);

// pairs
lua_pushnil(L);
while(lua_next(L, index) != 0) {
    // key 在 -2
    // value 在 -1
    lua_pop(L, 1);
}

// ipairs
for(i = 1; ; i++) {
    lua_rawgeti(L, index, i);
    if(lua_isnil(L, -1)) {
        lua_pop(L, -1);
        break;
    }
    // value 在 -1 
    lua_pop(L, -1);
}
```

特殊的表——全局表：

```
// 结果压栈
lua_getglobal(L, name);

// 将栈顶元素保存到全局表中的 name 处，出栈
lua_setglobal(L, name);
```

特殊的表——注册表

见 [lua 注册表](https://www.yvanhom.com/2016/06/06/lua-register-table/)。

特殊的表——索引表

索引表是特殊的注册表，key 都是整数

```
// 对栈顶元素生成索引，返回索引值
int r = lua_ref(L, LUA_REGISTRYINDEX);

// 解除 r 处的索引
luaL_unref(L, LUA_REGISTRYINDEX, r);

// 索引值获取，压栈
lua_rawgeti(L, LUA_REGISTRYINDEX, r);
```
