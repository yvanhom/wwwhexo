---
uuid:             5446b72d-fb78-4555-8512-fac1b148faf7
layout:           post
title:            '吐槽 lua 的栈操作'
slug:             lua-stack-operations
subtitle:         null
date:             '2016-05-31T05:45:42.000Z'
updated:          '2016-05-31T06:37:30.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---

Lua 语言跟 C 语言的参数交互使用到的是栈，一个虚拟的栈，看 Lua 的语言参考手册，这个栈的大小最大是 20，看起来很小，似乎放不下多少东西，但事实上，在 C 语言环境中，我们也很少把很多东西放到栈上去，所以 20 已经措措有余。

Lua 是一门很容易学习的语言，但一旦真要跟 C 语言交互，一下子就复杂起来了，毫无疑问，这个栈，加大了 Lua 的理解难度，那些函数的命名也是很有混淆性，这里来愉快地谈谈它们。

栈中元素的索引，从栈底开始向栈顶是 1，2，3，4……从栈顶开始到栈底是 -1，-2，-3……。数据的进出是 LIFO，后进先出，先压入的数据在栈底，后压入的数据在栈顶，先弹出栈顶的数据，再慢慢往栈底弹。

这里，都还好，都还很清晰。

简单的压栈函数：

* `lua_pushnil(L)` 压入 nil
* `lua_pushnumber(L, n)`
* `lua_pushinteger(L, n)`
* `lua_pushstring(L, s) `
* `lua_pushlstring(L, s, size)`
* `lua_pushfstring(L, fmt, ...)`
* `lua_pushboolean(L, b)` b 是整数，非 0 为真
* `lua_pushlightuserdata(L, point)`
* `lua_pushcfunction(L, f)` C 函数，用到 `lua_pushcclosure`
* `lua_pushcclosure(L, f, n)` C 闭包，n 指闭包的参数个数
* `lua_newtable(L)` 创建一个新表，压入栈中

`lua_push*string` 会返回压入的字符串。

到这里还是很明了，就是把一个数据压入栈。

获取栈中的数据：

* `lua_tonumber(L, i)`
* `lua_tointeger(L, i)`
* `lua_tostring(L, i)`
* `lua_tolstring(L, i, size)`
* `lua_toboolean(L, i)`
* `lua_topointer(L, i)`
* `lua_touserdata(L, i)`
* `lua_tocfunction(L, i)`
* `lua_touserdata(L, i)`

这里依然明了，都是获取数据，不改变栈的大小，栈中的内容。

可能栈中的数据不是所需要的类型，所以在获取前可能需要做检查。

* `lua_isnumber(L, i)`
* `lua_isinteger(L, i)`
* `lua_isstring(L, i)`
* `lua_iscfunction(L, i)`
* `lua_isuserdata(L, i)`
* `lua_istable(L, i)`
* `lua_islightuserdata(L, i)`
* `lua_isboolean(L, b)`
* `lua_isnil(L, i)`

要检查，又要获取，能不能合起来？

* `luaL_checknumber(L, i)`
* `luaL_checkinteger(L, i)`
* `luaL_checkstring(L, i)`
* `luaL_checklstring(L, i, size)`

`luaL_check*` 函数预计用到的是 `assert`，当检查失败，直接报错退出，成果就返回需要的数值。

简单的说完了，来开始说复杂的。

* `lua_getpop(L, n)` 获取栈的大小
* `lua_setpop(L, n)` 设置栈的大小，大了丢弃，小了就填 nil，可以为负，抛弃之上的数据，n处的数据总是保留，最终 n 处的数据变成了栈顶

* `lua_pushvalue(L, index)`，push*，这是把第二个参数的数据压入栈吧？是，也不是。这是把栈中第 index 的数据压入栈，相当于做了一次复制，天啊，能不能换个名字，多混淆啊。
* `lua_rotate(L, index, count)` 从 index 向栈顶轮转 n 个位置。
* `lua_insert(L, index)` 把栈顶数据插入 index 处，index 到栈顶的数据向上挪一位，栈的大小不变，相当于 lua_rotate(L, index, 1)
* `lua_copy(L, from, to)`
* `lua_remove(L, index)` 删除
* `lua_replace(L, index)` 弹出栈顶的数据，替换掉 index 处的数据
* `lua_pop(L, count)` 简单地弹出 count 个数据

* `lua_settable(L, index)` 为 table 的成员赋值，table 在第 index 处，value 在栈顶（-1），key在栈顶下（-2），然后 key 和 value 会弹出。

一般是会这样：

```lua
lua_newtable(L)
lua_pushstring(L, "key")
lua_pushstring(L, "value")
lua_settable(L, -3)
```

* `lua_setglobal(L, name)` 才栈顶弹出一个数据 ，赋给 _G[name]
* `lua_seti(L, index, i)`
* `lua_setfield(L, index, name)`
* `lua_rawset(L, index)`
* `lua_rawseti(L, index, i)`
* `lua_rawsetp(L, index, name)`

* `lua_gettable(L, index)` 取出 table 中的成员，table 在 index 处，key 在栈顶，运行后 key 被弹出，压入 value。
* `lua_getglobal(L, name)` 把 _G[name] 压入栈
* `lua_geti(L, index, i)`
* `lua_getfield(L, index, name)`
* `lua_rawgeti(L, index, i)`
* `lua_rawgetp(L, index, name)`

天啊，这么多相似的函数，仅仅是有一点不同，而且涉及到的压栈，出栈都有，看晕了。

晕完，来操作一些数据：

* `lua_call(L, nargs, nresults)` 调用 lua 函数，首先函数先入栈，然后依次压入参数，参数个数是 nargs。调用后函数和参数都弹出来，然后压入 nresults 个返回值。
* `lua_pcall(L, nargs, nresults, 0)` 安全模式，最后一个是如何处理错误

说了这么久，好像还没说道 L 指的什么，L 指 lua_State，可以 C 程序自己产生，也可以由 lua 程序调用 C 函数时传递。

C 程序自己管理 lua_State:

* L = lua_newstate()
* lua_close(L)

lua 程序调用 C 函数，这个说复杂，也不复杂

```C
#include <lib.h>
#include <lauxlib.h>
/* 定义函数 */
static int add(lua_State* L) {
    double a = luaL_checknumber(L, -1);
    double b = luaL_checknumber(L, -2);
    lua_pushnumber(L, a+b);
    return 1;        // 返回返回值的个数
}

static struct luaL_Reg add_table[] = {
    { "add", add },

    { NULL }
};

int luaopen_libadd(lua_State *L) {
    libL_newlib(L, add_table); // 创建一个表，设置函数映射，然后压栈
    return 1; // 返回值个数是 1 个 
}
```

写好 C 的程序后，编译成 libadd.so 文件，然后在 lua 程序中使用：

```lua
local la = require("libadd") -- 这里会调用 luaopen_libadd
print(la.add(10, 20))
```

其实 libL_newlib 是一个宏，定义如下：

```C
#define luaL_newlibtable(L,l)   \
   (luaL_checkversion(L), luaL_newlibtable(L,l), luaL_setfuncs(L,l,0))
```

本质是创建一个表，压入栈，`luaL_newlibtable` 跟 `lua_newtable` 的不同，只在设置了初始 table 的大小，最终不同的还是 `luaL_setfuncs`，将这些 cfunction 以某种方式让 lua 程序能够直接调用。


