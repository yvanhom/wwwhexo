---
uuid:             d92c8a95-da1d-4504-ab3f-d8f53a38ec79
layout:           post
title:            通过/var/log/wtmp查看登陆记录
slug:             get-log-infos
subtitle:         null
date:             '2015-03-18T21:58:28.000Z'
updated:          '2015-07-29T22:37:33.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


**/var/log/wtmp**是一个二进制文件，记录每个用户的登录次数和持续时间等信息。

使用**last**命令读取该文件：

```
[root@de18 ~]# last
root     pts/3        192.168.3.215    Thu Mar 19 01:47   still logged in   
root     pts/4                         Wed Mar 18 07:34 - 07:34  (00:00)    
root     pts/3        192.168.3.215    Wed Mar 18 07:10 - 13:27  (06:17)    
root     pts/2        :0.0             Thu Mar 12 05:19   still logged in   
root     pts/1        :0.0             Thu Mar 12 02:27   still logged in   
root                  :0               Thu Mar 12 02:27   still logged in
```

按登陆时间从近到远输出登陆信息，每行五列，从左到右分别是登陆用户名，登陆使用的tty，源IP地址，登陆时间，退出时间。

也可以使用**who /var/log/wtmp**读取部分信息：

```
root     :0           2015-03-12 02:27
root     pts/1        2015-03-12 02:27 (:0.0)
root     pts/2        2015-03-12 05:19 (:0.0)
root     pts/3        2015-03-18 07:10 (192.168.3.215)
root     pts/4        2015-03-18 07:34
root     pts/3        2015-03-19 01:47 (192.168.3.215)
```

与last相反，是按登陆时间从远到近输出，且少了退出时间。

使用**who**查看当前还在线上的用户：

```
[root@de18 ~]# who
root     :0           2015-03-12 02:27
root     pts/1        2015-03-12 02:27 (:0.0)
root     pts/2        2015-03-12 05:19 (:0.0)
root     pts/3        2015-03-19 01:47 (192.168.3.215)
```

猜测，前三个是系统启动后自动使用的，只有第四个是通过ssh登陆的，也就是我的登陆信息。



