---
uuid:             d8336c91-2085-404b-8444-e0737996bf09
layout:           post
title:            '尝试 loop 设备和软 RAID'
slug:             losetup-and-mdadm
subtitle:         null
date:             '2016-09-18T22:06:11.000Z'
updated:          '2016-09-18T22:06:11.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---

闲来蛋疼，尝试下使用多个文件组软阵列。

首先创建多个大小一致的文件：

```sh
dd if=/dev/zero of=./image1.img bs=1M count=1024
dd if=/dev/zero of=./image2.img bs=1M count=1024
dd if=/dev/zero of=./image3.img bs=1M count=1024
dd if=/dev/zero of=./image4.img bs=1M count=1024
```

用这些文件配置出 loop 设备：

```sh
sudo losetup /dev/loop1 ./image1.img
sudo losetup /dev/loop2 ./image2.img
sudo losetup /dev/loop3 ./image3.img
sudo losetup /dev/loop4 ./image4.img
```

组软阵列：

```sh
sudo mdadm --create /dev/md0 --level=0 --raid-devices=4 /dev/loop1 /dev/loop2 /dev/loop3 /dev/loop4
```

查看阵列信息：

```sh
sudo mdadm -D /dev/md0
```

格式化文件系统，并挂载：
```sh
sudo mkfs.ext4 /dev/md0
sudo mount /dev/md0 /mnt
```

随便操作操作：

```sh
sudo mkdir /mnt/test
sudo dd if=/dev/zero of=/mnt/test/disk bs=256K count=100
```

接下来是反操作：

```sh
# 卸载文件系统
sudo umount /mnt

# 删除软阵列
sudo mdadm -S /dev/md0

# 删除 loop 设备
sudo losetup -d /dev/loop1
sudo losetup -d /dev/loop2
sudo losetup -d /dev/loop3
sudo losetup -d /dev/loop4
```
