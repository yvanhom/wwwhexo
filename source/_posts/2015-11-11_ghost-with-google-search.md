---
uuid:             7f6c34b4-a573-42af-a0fb-acd450985596
layout:           post
title:            '为 Ghost 增加 Google 搜索'
slug:             ghost-with-google-search
subtitle:         null
date:             '2015-11-11T03:43:35.000Z'
updated:          '2015-11-11T03:43:35.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

参考 [Adding a Bing Search Box to a Website](http://rgardler.github.io/2015/07/25/adding-bing-search-to-a-website/) 和 [Add a Simple Google, Yahoo! or Bing Search Box to Your Website](http://www.developerdrive.com/2012/08/add-a-simple-google-yahoo-or-bing-search-box-to-your-website/)。这两篇文章更详细。

使用前提当然是需要网站被搜索引擎收录，只有收录了，才能使用搜索引擎提供的搜索功能。

这里集中讲 Ghost 博客中增加 Google 搜索引擎，方法也可以应用到别的博客系统，别的搜索引擎。

以本网站为例，在 Ghost 中新建文章

**TITLE** Search www.yvanhom.com

**CONTENT** 

```html
<form method="get" action="https://www.google.com/search">
    <input type="text" placeholder="Search..." name="q" value="" /> 
    <input type="hidden" name="sitesearch" value="www.yvanhom.com" />
</form>
```

**SLUG** googlesearch

**OTHER** select `Turn this post into a static page`

然后在 Navigation 增加 `Search` 对应 `https://www.yvanhom.com/googlesearch`。

完成后的效果见本[网站](/)和[搜索链接](/googlesearch)。
