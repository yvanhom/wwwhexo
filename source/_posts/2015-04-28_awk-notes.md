---
uuid:             f7a88e51-7dc3-438b-955d-a0a049fc0fae
layout:           post
title:            'awk 脚本速记'
slug:             awk-notes
subtitle:         null
date:             '2015-04-28T22:57:11.000Z'
updated:          '2015-07-29T22:35:26.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---


1. awk 的脚本语言跟 C 语言很相似: 
   - { } 划分代码块
   - if() {} else {} 语法
   - 循环语法 do {} while() 或者 for(;;) {}
   - 数值计算
   - 比较跟布尔运算
   - printf 语法
2. 需要注意的地方： 
   - 语句末尾不需要加分号
   - awk 的列变量序号从 1 开始计数
   - 字符串可以使用 == 和 != 进行比较
   - BEGIN{} 表示在列处理之前运行，END{} 表示在列处理后运行

给个求第一列数值的最大值的例子如下：

```
BEGIN {
  max = -1 
}
{
  if(max < $1) {
    max = $1 
  } 
}
END {
  printf("max value: %.2f", max) 
}
```



