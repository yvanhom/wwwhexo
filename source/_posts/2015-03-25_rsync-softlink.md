---
uuid:             4eb66349-780a-4684-8eb2-7be3f41961ef
layout:           post
title:            rsync与软链接
slug:             rsync-softlink
subtitle:         null
date:             '2015-03-25T23:40:12.000Z'
updated:          '2015-07-29T22:36:53.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


使用rsync同步文件时，常使用-a选项，该选项默认将软链接同步为软链接，因为该选项已经把-l选项包含进去，如果需要跟踪软链接，将软链接同步为正常文件，此时可以使用-L选项。



