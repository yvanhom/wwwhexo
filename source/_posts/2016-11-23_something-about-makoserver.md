---
uuid:             816279ac-5cb3-4ef3-801d-45e7ef2f4214
layout:           post
title:            'MakoServer 的一些坎'
slug:             something-about-makoserver
subtitle:         null
date:             '2016-11-23T21:23:24.000Z'
updated:          '2016-11-24T07:26:48.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site
  - lua

---

As a compact application and web server, the Mako Server helps developers rapidly design IoT and web applications. 

玩上 MakoServer 的主要原因之一是，我喜欢 **Lua** 语言，所以尝试一下基于 **Lua** 语言的后端设计，找到的有三款，一款是 [CGILUA](https://github.com/keplerproject/cgilua)，提供 Lua Pages 和 Lua Scripts，可惜我看了文档后不清楚该怎么用，一款是 [Ladle web server](https://github.com/danielrempel/ladle)，也是 Lua Pages 和 Lua Scripts，太过简陋，最后一款是 [Barracuda Application Server](https://barracudadrive.com/) 以及 [MakoServer](https://makoserver.net/)，提供 Lua Server Pages，相对比较完整。

玩上后，写了一个 [Openshift Do-It-Yourself with MakoServer](https://github.com/padicao2010/openshift-diy-makoserver)，以及基于前者的 [Personal Finance Manager](https://github.com/padicao2010/openshift-diy-makoserver/tree/pfm)。

这里把这个过程中遇到的一些坎总结一下：

### 配置少，一些默认项

比如：

* MakoServer 的程序目录在 `/home/yvanhom/MakoServer`
* Web 目录在 `/home/yvanhom/Web`
* 在 `/home/yvanhom` 目录下运行 `/home/yvanhom/MakoServer/mako -l::Web -d`

此时：

* SQLite 的目录是在程序目录下的子目录 data 里，比如 `sqlutil.open("my")` 会生成 `/home/yvanhom/MakoServer/data/my.sqlite.db` 文件。
* Lua 库目录在运行目录下，比如 `require "libs.pfm"` 会寻找 `/home/yvanhom/libs/pfm.lua` 文件。
* -d 即 Daemon 模式，此时运行日志 `mako.log` 保存在运行目录下，即 `/home/yvanhom/mako.log`

### OpenShift 只支持 8080 端口

MakoServer 默认在 `.openports` 的时候会检查 http 端口和 https 端口，部署在 OpenShift 上时，https 端口总是检查失败，导致不能启动程序，为此我不得不把 `mako.zip/.openports` 文件中关于 https 的部分全部注释掉，然后重新打包。

结果可见 [Openshift Do-It-Yourself with MakoServer](https://github.com/padicao2010/openshift-diy-makoserver)。

### 目录的垃圾回收

[MakoServer](https://realtimelogic.com/ba/doc/) 的文档中，关于目录部分有类似的一段代码

```
testdir = ba.create.dir("")
testdir:insert()
testdir:setfunc(testfunc)
```

本着少用全局变量的原则，我把 `testdir` 加上了 `local`，结果总是出问题，折腾了几天，才发现是垃圾回收把这个目录给回收了，毕竟垃圾回收并不是马上回收，所以还会正常运行一段时间才出问题。

吐槽一下，一般 `testdir:insert()` 不就把 `testdir` 交给父亲节点管理了嘛，怎么还会有问题。结果没有，所以还是得用全局变量，不然给垃圾回收了，就失效没用了。

这里不得不说 MakoServer 的文档写得不好，本应该写清楚的，结果耗费了几天的时间来纠结这个问题。

### 目录的优先级问题

按上边的代码创建了根目录后，你会发现这段代码不起作用，因为默认的优先级是 0，文件目录挂载的优先级也是 0，结果导致会先在文件目录上找 `index.html`、`index.lsp`等，而不是执行 `testfunc`。

又是文档的疏忽，还好在这点上我很快就找到了问题所在。

### 复杂的 Lua 环境

有三种环境，Lua 环境，app 环境，请求环境。

* 在 `.preload` 中的是 app 环境，所有 .preload 的全局变量都会保存在 app 表里，而且 app 表里也有 app 变量指向自身。
* 在响应请求时，是请求环境，有 app、request、response 等全局变量，请求环境中新的全局变量都会在请求结束后消失。
* 部分回掉函数的第一个参数需要传递 _ENV，即传递环境，这个时候，就必须得有这个参数，不能省略。

就这些吧。理清之后，感觉 MakoServer 以及 LSP(Lua Server Pages) 挺好用的，是个完整的轻量级的解决方案，对 Http 的封装很直观，支持数据库 SQLite，支持 Json、XML 等格式，可以使用 MD5、SHA1、SSL 等，再配合 Lua 的简单高效强大，基本可以满足个人网络应用的需求。
