---
uuid:             b34355da-2c86-4145-bc2e-d20c6626798c
layout:           post
title:            'Hello, World!'
slug:             hello-world-3
subtitle:         null
date:             '2014-07-23T04:32:37.000Z'
updated:          '2015-07-29T22:45:44.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - other

---


C++的Hello, World!程序如下：

```
#include<iostream>
using namespace std;
 
int main() {
    cout << "Hello, World!" << endl;
    return 0;
}
```

 



