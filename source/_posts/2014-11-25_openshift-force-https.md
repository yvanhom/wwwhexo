---
uuid:             27adf74c-30a0-4951-a80d-8133df639479
layout:           post
title:            设置OpenShift，强制使用https
slug:             openshift-force-https
subtitle:         null
date:             '2014-11-25T08:07:58.000Z'
updated:          '2015-07-29T22:41:53.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


由于国内把rhcloud.com的http端口给墙了，造成了很多不便，无意中发现可以通过添加.htaccess文件来强制从http转向https，而https端口并没有被墙。

参考[链接](https://help.openshift.com/hc/en-us/articles/202398810-How-to-redirect-traffic-to-HTTPS- "openshift的http转https")。

这里补充一点，链接中没有说明php的web root目录在哪，而我又是php的门外汉，所以就瞎摸索，貌似是app-root/data/current目录，起码在这个目录创建.htaccess文件，加入连接中的文本，便能强制使用https了。

```
RewriteEngine on 
RewriteCond %{HTTP:X-Forwarded-Proto} !https 
RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R,L]
```

今天又改了一个app，结果web root目录跑到了app-root/runtime/repo这里。



