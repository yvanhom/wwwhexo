---
uuid:             b8aa2c1e-2aaf-4c19-87d8-c7f42ee24d61
layout:           post
title:            google字体本地化
slug:             google-fonts-replacement
subtitle:         null
date:             '2015-01-26T03:32:50.000Z'
updated:          '2016-11-14T22:25:08.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


使用[bolt](http://bolt.cm "Easy for editors, and a developer's dream cms")的时候，经常遇到一些主题（theme）使用google的字体，可是谷歌被墙得厉害，只能做些替换，一种可行方案便是使用useso的字体库进行替换。

查找是否有使用google字体库：

```
# -r 表示对这个目录进行遍历匹配
# -o 只输出匹配的部分，只是要找出文件便可以，输出整行有时很难看
grep -o -r fonts.googleapis.com ./
```

然后对每个文件进行替换：

    sed -i -e 's/fonts.googleapis.com/fonts.useso.com/g' filepath

不过这样做存在一个问题，那便是useso.com没有https，于是当网站使用https访问，会使用https访问useso.com，于是就卡住很长时间直到连接超时，这个比访问googleapis还慢。

可以在使用useso.com中强制加上http，避免使用https，但是在@import中包含http访问时，在firefox下，会直接拒绝访问。

折腾来折腾去，发现我所需要的字体很少，于是就直接把字体下载到空间上，不再访问useso或者googleapis了。

首先，我的空间使用的字体语句是：

```
@import url("https://fonts.googleapis.com/css?family=Noto+Sans:700,700italic");
```

将上边链接的文件下载下来，保存为sourcesans.css文件，文件内容如下：

```
@font-face {
  font-family: 'Open Sans Condensed';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Cond Light'), local('OpenSans-CondensedLight'), url(http://fonts.gstatic.com/s/opensanscondensed/v10/gk5FxslNkTTHtojXrkp-xF1YPouZEKgzpqZW9wN-3Ek.woff) format('woff');
}
@font-face {
  font-family: 'Open Sans Condensed';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Condensed Bold'), local('OpenSans-CondensedBold'), url(http://fonts.gstatic.com/s/opensanscondensed/v10/gk5FxslNkTTHtojXrkp-xONSK5BxN3NFS4EJkViHIqo.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: normal;
  font-weight: 400;
  src: local('Source Sans Pro'), local('SourceSansPro-Regular'), url(http://fonts.gstatic.com/s/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: normal;
  font-weight: 700;
  src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(http://fonts.gstatic.com/s/sourcesanspro/v9/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: italic;
  font-weight: 400;
  src: local('Source Sans Pro Italic'), local('SourceSansPro-It'), url(http://fonts.gstatic.com/s/sourcesanspro/v9/M2Jd71oPJhLKp0zdtTvoMzNrcjQuD0pTu1za2FULaMs.woff) format('woff');
}
```

将woff字体和css文件复制到空间特定目录下，修改特定项使用它们。

上边的import改为：

    @import url("sourcesans.css");

sourcesans.css文件改为：

```
@font-face {
  font-family: 'Open Sans Condensed';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Cond Light'), local('OpenSans-CondensedLight'), url(../fonts/OpenSans-CondensedLight.woff) format('woff');
}
@font-face {
  font-family: 'Open Sans Condensed';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Condensed Bold'), local('OpenSans-CondensedBold'), url(../fonts/OpenSans-CondensedBold.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: normal;
  font-weight: 400;
  src: local('Source Sans Pro'), local('SourceSansPro-Regular'), url(../fonts/SourceSansPro-Regular.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: normal;
  font-weight: 700;
  src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(../fonts/SourceSansPro-Bold.woff) format('woff');
}
@font-face {
  font-family: 'Source Sans Pro';
  font-style: italic;
  font-weight: 400;
  src: local('Source Sans Pro Italic'), local('SourceSansPro-It'), url(../fonts/SourceSansPro-It.woff) format('woff');
}
```

如此，访问空间使用的便是空间上的字体文件，不再需要使用googleapis或者useso了。

如果空间使用CDN加速，那么这些静态文件一般可以被CDN缓存而不需要每次都访问源站点，达到加速效果。



