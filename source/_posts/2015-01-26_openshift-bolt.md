---
uuid:             1c3bab40-4ea7-4b2a-96a0-340d8c5d83fb
layout:           post
title:            在openshift上部署升级bolt
slug:             openshift-bolt
subtitle:         null
date:             '2015-01-26T22:15:21.000Z'
updated:          '2015-07-29T22:38:39.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


在openshift上部署bolt最简单的便是直接把bolt的下载包直接解压push上去，然后安装。

```
# 1, 先clone下来
git clone ssh://XXXXXXXX.git
 
# 2, 解压bolt的下载包并放置到指定目录
# 省略
# tar xvzf bolt-X.X.X.tar.gz
# mv bolt-X.X.X/* XXX

# 3, push上去
git add --all .
git commit -m "安装bolt"
git push
```

git push后所有文件保存到app-root/repo下，这个目录的文件在每次push后都会删除，而默认的bolt配置和数据都保存到这个目录下，所以这种方式很糟糕，在升级的时候更糟糕，会丢失数据。

所以最后数据，配置，以及其它希望在升级的时候保留的文件等都保存到app-root/data目录下，然后通过软链接供bolt使用。这里，我将上传的文件files，主题theme，配置app/config，数据app/database，扩展extensions，翻译app/resources/translations/zh_CN等都保存到app-root/data目录下，并在原有位置创建软链接。app-root/data目录有以下内容：

    config database extensions files theme zh_CN

需要将bolt下载包的内容稍做修改，替换特定目录为软链接，使用app-root/data中的内容。所创建的软链接有：

```
# bolt下载包目录
extensions -> ../data/extensions
files -> ../data/files
theme -> ../data/theme
 
# bolt下载包目录/app
config -> ../../data/config
database -> ../../data/database
 
# bolt下载包目录/app/resources/translations/zh_CN
zh_CN -> ../../../../data/zh_CN
```

如下是我使用的脚本文件，如果您要使用的话，需要根据需要进行修改，运行前先备份，避免损失：

```
BOLT_DIR="bolt-2.0.5"            # bolt下载包目录
DATA_DIR="../data"               # extensions，files，theme使用的软链接相对路径
DATA2_DIR="../../data"           # config，database使用的软链接相对路径
DATA4_DIR="../../../../data"     # zh_CN使用的软链接相对路径
RH_DATA_DIR="/var/lib/XXXXXXXX/app-root/data"        # 应用的ssh目录
RH_BOLT="rh_bolt"                # 应用的ssh Host，我在.ssh/config中配置了的
EXTENSIONS="extensions"
CONFIG="config"
CONFIG_SRC="app/config"
DB="database"
DB_SRC="app/database"
THEME="theme"
FILES="files"
ZH_CN="zh_CN"
ZH_CN_SRC="app/resources/translations/zh_CN"
 
echo goto ${BOLT_DIR}
cd ${BOLT_DIR}
 
echo == change extensions directory
# 把目录替换为软链接
if [ -d ${EXTENSIONS} ]; then
    rm -rf ./${EXTENSIONS}
fi
ln -s ${DATA_DIR}/${EXTENSIONS} ./
 
# 更新dist文件，把目录替换为软链接
echo == change config direcotry
if [ -d ${CONFIG_SRC} ]; then
    scp ./${CONFIG_SRC}/*.dist ${RH_BOLT}:${RH_DATA_DIR}/${CONFIG}/
    rm -rf ./${CONFIG_SRC}
fi
ln -s ${DATA2_DIR}/${CONFIG} ./${CONFIG_SRC}
 
# 把目录替换为软链接
echo == change database directory
if [ -d ${DB_SRC} ]; then
    rm -rf ./${DB_SRC}
fi
ln -s ${DATA2_DIR}/${DB} ./${DB_SRC}
 
# 更新系统主题，如果不需要更新，注释掉相应的scp语句，把目录替换为软链接
echo == change themes
if [ -d ${THEME} ];  then
        # 替换google字体
    sed -i -e 's/fonts.googleapis.com/fonts.useso.com/g' ${THEME}/base-2014/_header.twig
    ssh ${RH_BOLT} rm -rf ${RH_DATA_DIR}/${THEME}/base*
    scp -r ./${THEME}/* ${RH_BOLT}:${RH_DATA_DIR}/${THEME}/
    #ssh ${RH_BOLT} cp -r ${RH_DATA_DIR}/${THEME}/base-2014 ${RH_DATA_DIR}/${THEME}/my-theme
    rm -rf ./${THEME}
fi
ln -s ${DATA_DIR}/${THEME} ./
 
# 将目录替换为软链接
echo == change files
if [ -d ${FILES} ]; then
    scp ./${FILES}/* ${RH_BOLT}:${RH_DATA_DIR}/${FILES}/
    rm -rf ./${FILES}
fi
ln -s ${DATA_DIR}/${FILES} ./
 
# 将目录替换为软链接
echo == change zh_CN
if [ -d ${ZH_CN_SRC} ]; then
    # ssh ${RH_BOLT} rm -rf ${RH_DATA_DIR}/${ZH_CN}
    # scp -r ./${ZH_CN_SRC} ${RH_BOLT}:${RH_DATA_DIR}/${ZH_CN}
    rm -rf ./${ZH_CN_SRC}
fi
ln -s ${DATA4_DIR}/${ZH_CN} ./${ZH_CN_SRC}
 
echo == go back
cd -
```

最后按上述流程push更新便可以了。

更新后可能还需要根据需要再做点人工修改，比如新版本可能增加一些配置选项，比较config/*.dist文件跟配置文件，看哪些配置需要更新，比如比较翻译文件，看哪些翻译更新了等等。

本人已经顺利地从bolt-2.0.0更新到bolt-2.0.5。bolt现在处于高速发展阶段，更新频繁，以后还是等大版本出现才更新，省事。

对上述openshift上的bolt备份很简单，把app-root/data目录打包，并下载，上传到dropbox等变备份成功了，由于app-root/data包含了主题，上传文件，所以备份的数据还不小，根据需要看是否不备份这些。



