---
uuid:             353b109a-779f-4aaa-a941-ccdc05fe7965
layout:           post
title:            'FreeBSD下的CMake -- HelloWorld实例'
slug:             freebsde-cmake-helloworld-example
subtitle:         null
date:             '2014-11-21T05:23:13.000Z'
updated:          '2015-07-29T22:42:09.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---



## 一，目录树

源码目录树如下：

```
.
├── CMakeLists.txt
├── COPYRIGHT
├── README
├── doc
│   └── hello.txt
├── runhello.sh
└── src
    ├── CMakeLists.txt
    ├── hello.c
    ├── hello.h
    └── main.c
```

其中源码目录都放在src目录下，目标是通过CMake将hello.c编译成libhello.so共享库，将main.c编译成testhello（使用先前的共享库），然后将文件安装到传统约定的目录下。

CMakeLists.txt是CMake默认读取的文件，基本上每个源代码目录都需要一个，这里使用两个，顶层的CMakeLists.txt指明工程信息，src下的CMakeLists.txt指明如何编译。


## 二，编译设置

顶层CMakeLists.txt中，将源码目录加入工程，CMake根据这个会到指定的目录下查找CMakeLists，然后继续编译。

```
# 指定最小版本号，这里随意，不加会报WARNING，强迫症看不管WARNING，所以就随便加个
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
# 指定项目名称
PROJECT(HELLO)
# 加入子目录
ADD_SUBDIRECTORY(src)
```

ADD_SUBDIRECTORY(src)将src加入工程，在编译目录下，也会创建这个目录，并把编译的中间数据保存到这个目录下，可以通过再增加个参数来将数据放到别的目录。

src下的CMakeLists.txt中，写入如何编译。

```
# 指定Build时，生成的可执行文件和库文件放置的位置
# PROJECT_BINARY_DIR一般是运行cmake的目录
SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
SET(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)
 
# 设置变量
SET(LIBHELLO_SRC hello.c)
# 编译共享库和静态库
ADD_LIBRARY(hello SHARED ${LIBHELLO_SRC}) 
ADD_LIBRARY(hello_static STATIC ${LIBHELLO_SRC})
# 这里需要让静态库和动态库同名，所以有以下设置，同名指的是libhello.so和libhello.a
SET_TARGET_PROPERTIES(hello PROPERTIES CLEAN_DIRECT_OUTPUT 1)
SET_TARGET_PROPERTIES(hello_static PROPERTIES CLEAN_DIRECT_OUTPUT 1)
# 指定静态库名称也是hello，否则将默认是hello_static
SET_TARGET_PROPERTIES(hello_static PROPERTIES OUTPUT_NAME "hello")
# 指定动态库的版本
SET_TARGET_PROPERTIES(hello PROPERTIES VERSION 1.0 SOVERSION 1)
# 貌似不能指定静态库的版本
#SET_TARGET_PROPERTIES(hello_static PROPERTIES VERSION 1.0 SOVERSION 1)
 
# 加入可执行文件
ADD_EXECUTABLE(testhello main.c)
TARGET_LINK_LIBRARIES(testhello hello)
```

由于testhello还使用到了别的共享库pthread，所以，需要增加别的链接库。

在CMake中一般先运行cmake –help-module FindXXX来看使用特定的共享库需要设置些什么，为了使用pthread，首先查找该使用哪个FindXXX模块，运行：

    # 在cmake的安装目录下，通过关键字thread查找，如果没找到，那么便换关键字 
    ls /usr/local/share/cmake/Modules/ | grep -i thread

结果显示：

```
CheckForPthreads.c 
FindOpenThreads.cmake 
FindThreads.cmake
```

最接近的便是FindThreads模块了，于是查看帮助信息：

    cmake --help-module FindThreads

显示：

```
FindThreads
-----------
 
This module determines the thread library of the system.
 
The following variables are set
 
::
 
 CMAKE_THREAD_LIBS_INIT     - the thread library
 CMAKE_USE_SPROC_INIT       - are we using sproc?
 CMAKE_USE_WIN32_THREADS_INIT - using WIN32 threads?
 CMAKE_USE_PTHREADS_INIT    - are we using pthreads
 CMAKE_HP_PTHREADS_INIT     - are we using hp pthreads
 
For systems with multiple thread libraries, caller can set
 
::
 
 CMAKE_THREAD_PREFER_PTHREAD
```

从上边可以看出FindThreads增加了几个变量，其中CMAKE*THREAD*LIBS_INIT变量便是我们需要的，于是：

```
# 查找需要的包
FIND_PACKAGE(Threads REQUIRED)
# 指定testhello链接pthread
TARGET_LINK_LIBRARIES(testhello ${CMAKE_THREAD_LIBS_INIT})
```

别的共享库可能需要增加包含目录，使用INCLUDE_DIRECTORIES，将帮助信息中的相应变量作为参数：

    INCLUDE_DIRECTORIES(dir1 dir2 ...)

可能需要增加链接目录，使用LINK_DIRECTORIES，将帮助信息中的相应变量作为参数：

    LINK_DIRECTORIES(dir1 dir2 ...)


## 三，安装设置

将文件安装到该去的目录，这个很重要。

cmake提供一个变量CMAKE_INSTALL_PREFIX，跟AutoTools的–prefix相似，都是安装目录的前缀，默认是/usr/local。

顶层CMakeLists.txt，增加如下安装要求：

```
# 使用相对路径来使用CMAKE_INSTALL_PREFIX
# 安装普通文件
INSTALL(FILES COPYRIGHT README DESTINATION share/doc/hello)
# 安装目录，如果是doc，那么会将整个目录复制过去，如果是doc/，那么只复制目录下的内容，也就是不包括doc目录
INSTALL(DIRECTORY doc/ DESTINATION share/doc/hello)
# 安装程序，这里会将runhello.sh赋予可执行权限
INSTALL(PROGRAMS runhello.sh DESTINATION bin)
```

src的CMakeLists.txt，增加如下安装要求：

```
# 该目录下的CMakeLists.txt生成三个目标，分别是
# 可执行程序RUNTIME：testhello，安装到bin目录，
# 共享库LIBRARY：hello，安装到lib目录
# 静态库ARCHIVE：hello_static，安装到libstatic目录
INSTALL(TARGETS testhello hello hello_static
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION libstatic)
 
# 头文件是普通文件，安装到include目录
INSTALL(FILES hello.h DESTINATION include)
```

##  四，动起来

```
# 创建并进入编译目录
mkdir build
cd build
# 生成Makefile
cmake -DCMAKE_INSTALL_PREFIX=/tmp/hello ../
# 编译
make
# 安装
make install
```

编译后，如下是build的文件目录树，生成了好多中间文件：

```
build
├── CMakeCache.txt
├── CMakeFiles
│   ├── 3.0.2
│   │   ├── CMakeCCompiler.cmake
│   │   ├── CMakeCXXCompiler.cmake
│   │   ├── CMakeDetermineCompilerABI_C.bin
│   │   ├── CMakeDetermineCompilerABI_CXX.bin
│   │   ├── CMakeSystem.cmake
│   │   ├── CompilerIdC
│   │   │   ├── CMakeCCompilerId.c
│   │   │   └── a.out
│   │   └── CompilerIdCXX
│   │   ├── CMakeCXXCompilerId.cpp
│   │   └── a.out
│   ├── CMakeDirectoryInformation.cmake
│   ├── CMakeError.log
│   ├── CMakeOutput.log
│   ├── CMakeTmp
│   ├── Makefile.cmake
│   ├── Makefile2
│   ├── TargetDirectories.txt
│   ├── cmake.check_cache
│   └── progress.marks
├── Makefile
├── bin
│   └── testhello
├── cmake_install.cmake
├── lib
│   ├── libhello.a
│   ├── libhello.so -> libhello.so.1
│   ├── libhello.so.1 -> libhello.so.1.0
│   └── libhello.so.1.0
└── src
 ├── CMakeFiles
 │   ├── CMakeDirectoryInformation.cmake
 │   ├── hello.dir
 │   │   ├── C.includecache
 │   │   ├── DependInfo.cmake
 │   │   ├── build.make
 │   │   ├── cmake_clean.cmake
 │   │   ├── depend.internal
 │   │   ├── depend.make
 │   │   ├── flags.make
 │   │   ├── hello.c.o
 │   │   ├── link.txt
 │   │   └── progress.make
 │   ├── hello_static.dir
 │   │   ├── C.includecache
 │   │   ├── DependInfo.cmake
 │   │   ├── build.make
 │   │   ├── cmake_clean.cmake
 │   │   ├── cmake_clean_target.cmake
 │   │   ├── depend.internal
 │   │   ├── depend.make
 │   │   ├── flags.make
 │   │   ├── hello.c.o
 │   │   ├── link.txt
 │   │   └── progress.make
 │   ├── progress.marks
 │   └── testhello.dir
 │   ├── C.includecache
 │   ├── DependInfo.cmake
 │   ├── build.make
 │   ├── cmake_clean.cmake
 │   ├── depend.internal
 │   ├── depend.make
 │   ├── flags.make
 │   ├── link.txt
 │   ├── main.c.o
 │   └── progress.make
 ├── Makefile
 └── cmake_install.cmake
```

然后是安装目录树，可以看到文件都安装到指定的位置：

```
/tmp/hello
├── bin
│   ├── runhello.sh
│   └── testhello
├── include
│   └── hello.h
├── lib
│   ├── libhello.so -> libhello.so.1
│   ├── libhello.so.1 -> libhello.so.1.0
│   └── libhello.so.1.0
├── libstatic
│   └── libhello.a
└── share
    └── doc
        └── hello
            ├── COPYRIGHT
            ├── README
            └── hello.txt
```


##  五，问题

安装后，运行不了，原因是安装目录不是系统默认目录，共享库没有安装到系统目录，所以执行程序找不到。

然后根据这个链接[http://www.cmake.org/Wiki/CMake*RPATH*handling ](http://www.cmake.org/Wiki/CMake_RPATH_handling "CMake RPATH")中教的做，结果还是失败，cmake还是没在安装文件中加入RPATH。

不知道怎么解决，因为网上的文章基本上都是使用wiki中的办法。

只能退一步，避开这个问题，于是写了runhello.sh的脚本，设置LD*LIBRARY*PATH环境变量来设置共享库目录

```
#!/bin/sh
 
# 获取脚本所在目录
# 对变量$0进行字符串街区操作，将最右边开始匹配到/*的所有字符串删除
# 比如处理./runtest.sh得到./，处理/tmp/hello/bin/runtest.sh得到/tmp/hello/bin
# 将/tmp/hello/bin放到PATH变量中，处理得到的还是/tmp/hello/bin
BIN_DIR=${0%/*}
BIN=testhello
 
# 设置LD_LIBRARY_PATH为该共享库的路径
LD_LIBRARY_PATH=${BIN_DIR}/../lib ${BIN_DIR}/$BIN
```

不过遇到的这个问题还是得再想办法解决。



