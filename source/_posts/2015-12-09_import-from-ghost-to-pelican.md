---
uuid:             9ad1e7b5-c257-47aa-9acc-3f13abbc10ac
layout:           post
title:            '从 Ghost 导出数据到 Pelican'
slug:             import-from-ghost-to-pelican
subtitle:         null
date:             '2015-12-09T07:44:30.000Z'
updated:          '2015-12-09T08:03:04.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua
  - build-site

---

[Ghost](https://blog.ghost.org/) 是一套基于 Node.js 构建的开源博客平台，而 [Pelican](http://blog.getpelican.com/) 是采用了Python编写静态博客生成系统，都是非常好用的系统。Ghost 的优势是有一个网页编辑后台，Pelican 的优势是生成的静态博客资源占用少。

我的博客构建在 Ghost 上，突发奇想，试试 Pelican，于是对数据进行了一番转化，尝试了一下 Pelican。

首先从 Ghost 后台，点击 Labs -- Export 导出数据，格式是 Json，这里重命名为 ghost.json。

使用 Lua 处理 Json 数据的方法已经在 [Lua 的 JSON 处理](/2015/11/27/lua-deal-with-json/)中讲到。这里使用的同样是 [json4lua](https://github.com/craigmj/json4lua)。

转换代码如下：

```lua
-- ghost2pelican.lua
local json = require("json")

local infilename = arg[1]
local outdir = arg[2]

-- 从 Ghost Json 的数组格式转换成 KeyValue 格式，方便 Tag 的查找
local function mapTags(tags)
        local results = {}
        for i, t in ipairs(tags) do
                results[t.id] = t.name
        end
        return results
end

-- 读取 Json 文件
local function getFromGhostJson(filename)
        local file = io.open(filename)
        local str = file:read("*all")
        local db = json.decode(str).db[1].data
        return db.posts, mapTags(db.tags), db.posts_tags
end

-- 时间格式转换
-- 从 '2014-07-23T08:32:37.000Z' 到 '2014-07-23 08:32:37'
local function getDate(s)
        return ("%d-%02d-%02d %02d:%02d:%02d"):format(s:match("(%d+)-(%d+)-(%d+)T(%d+):(%d+):(%d+)"))
end

-- 创建文件
local function createFile(slug)
        return io.open(("%s/%s.md"):format(outdir, slug), "w")
end

local posts, tags, postTags = getFromGhostJson(infilename)

-- 获取文章 Tags
local function getTags(id)
        local result
        for i, v in ipairs(postTags) do
                if v["post_id"] == id then
                        local t = tags[v["tag_id"]]
                        if result then
                                result = result .. ", " .. t
                        else
                                result = t
                        end
                end
        end
        return result
end

-- 遍历文章，生成一个个文件
for i, p in ipairs(posts) do
        local file = createFile(p.slug)
        file:write(("Title: %s\n"):format(p.title))
        file:write(("Date: %s\n"):format(getDate(p["published_at"])))
        file:write(("Modified: %s\n"):format(getDate(p["updated_at"])))
        file:write(("Tags: %s\n"):format(getTags(p.id)))
        file:write(("Slug: %s\n"):format(p.slug))
        file:write(("\n%s"):format(p.markdown))
        file:close()
end
```

运行格式：

```sh
lua ghost2pelican.lua ghost的导出文件 输出文件目录
```

导出后，每篇文章对应一个文件，文件的名字便是文章的 Slug。

以下是一些补充说明：

* Ghost 中只有 tag，没有 Category，转换成 Pelican 后，所有文章都在 misc 分类下。
* Ghost 的 tag 有 name，slug 等属性，对英文 tag 来说，这二者是一样的，没什么问题。而我使用的则是 name 是中文，slug 是英文翻译，而 Pelican 也会做翻译，给的中文 tag，自动翻译成了拼音，目前找不到可以修改的地方。
* 没有做用户，作者等相关的转换。
* 我用 luajit 做的测试，使用 lua 应该也没问题。

使用 Pelican 生成的静态博客好难看，还是需要找主题，不然完全拿不出手。
