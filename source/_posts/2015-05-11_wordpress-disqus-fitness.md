---
uuid:             d562a2b7-9343-426e-a603-9ae49a6cac7d
layout:           post
title:            'WordPress Twenty Fifteen 主题与 Disqus 适配'
slug:             wordpress-disqus-fitness
subtitle:         null
date:             '2015-05-11T03:33:58.000Z'
updated:          '2015-07-29T22:34:49.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


WordPress 的新主题 Twenty Fifteen 使用 Disqus 评论系统时，不能很好地进行适配。

原因很简单，TF 主题中所有的东西都有一个框框包住，而 Disqus 评论增加的并没有，于是破坏了主题的美感。

所以修复也很简单，给 Disqus 的内容加上一个同样框框样式。

在 主题 – 编辑 中，编辑 style.css，在开头的注释后边加入：

```
/*disqus*/ 
div#disqus_thread {
  padding: 3% 10%;
  background-color: white;
  margin: 3% 0;
  box-shadow: 0 0 1px rgba(0,0,0,0.15);
} 
@media screen and (min-width: 38.75em) {
  div#disqus_thread {
    margin: 3% 7.6923%;
  }
} 
@media screen and (min-width: 59.6875em) {
  div#disqus_thread {
    margin: 3% 8.3333%;
  } 
}
```

升级主题经常把这个给覆盖了，再加上我这个站使用 CloudFlare CDN，缓存了 style.css，所以要看到升级后的不适配结果和修改后的适配结果，都得等一段时间才行。



