---
uuid:             466bcd00-29d3-4431-8930-1ee51ae72905
layout:           post
title:            SSH端口转发
slug:             ssh-port-redirection
subtitle:         null
date:             '2014-11-12T02:58:09.000Z'
updated:          '2015-07-29T22:43:08.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - freebsd
  - shell

---



## 一，使用端口转发来穿越各种墙

此次出差，使用的网络经过了层层防火墙，各种端口封禁。从外边只能ping过来，要直接远程ssh是做不到的。

但是ssh的端口转发提供了一个穿墙的好方法。


## 二，端口转发

### 1, 远程端口转发

命令：ssh -R 33333:localhost:22 remotehost

该命令会从本地机连接远程机remotehost，连接后建立端口转发，使得远程机可以使用33333端口来连接本地机的localhost:22，于是远程机也能远程ssh本地机。

### 2, 本地端口转发

命令：ssh -L 33333:localhost:8080 remotehost

该命令会从本地机连接远程机，连接后建立端口转发，使得本地机能够通过33333端口通过远程机访问远程机上的localhost:8080，适用于远程机只将8080端口开放给localhost。


## 三，其它有用参数

- -f，在执行命令前进入后台
- -N，不执行远程命令
- -g，允许远程机子使用转发的端口

我这次使用的命令是：

    ssh -fNg -R 24680:localhost:22 XXX


## 四，其它转发

### 1, 动态转发

命令：ssh -D <local port> <remote server>

该命令相当与建立一个ssh代理，所有经过<local port>的数据都会交给<remote server>操作，所以将这个端口设置为浏览器代理后，浏览器对别的网站的访问都会通过这条通道转变为<remote server>对该网站的访问。

不过ssh代理，我还没用过。

### 2, X协议转发

命令：ssh -X <remote server>

这个命令建立的连接，可以使得在远程运行的图形程序在本地显示，从而可以运行图形应用程序。

很好用，很常用的一个命令。

比如，在远程连接到虚拟机后，可以运行wireshark图形界面来抓取并查看虚拟机上的网络包。



