---
uuid:             035f0bf6-837b-4490-930e-3065d1397fa7
layout:           post
title:            给PHP空间安装goagent当http代理
slug:             goagent-php-for-proxy
subtitle:         null
date:             '2015-01-26T09:03:56.000Z'
updated:          '2015-07-29T22:38:51.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


本来goagent是使用Google App Engine的，可是最近google被墙得厉害，于是在免费的PHP空间[byethost.com](http://byethost.com "免费php空间")上搭建了goagent，当作备用。

不过免费空间一般都禁用代理，一发现代理便封号删数据，真心伤不起，所以使用需谨慎。之前我在OpenShift使用snova代理，一直没事，可是后来听说OpenShift是禁止代理使用的，于是就把snova删掉，我还是很害怕RedHat把我的号给封了，那么我的博客就完蛋了。

流程还是比较简单的。

首先下载goagent的文件压缩包，解压出server/php目录下的文件，可以看到只有两个php文件，还真是轻量级。

打开index.php文件，修改密码，我可不想被人一不小心发现了拿去使用：

```
$__version__ = '3.1.2'; 
$__password__ = '******';
```

然后将两个php文件index.php跟relay.php上传到php空间中，于是就http代理服务器就搭建好了，真心快。

http代理的客户端还是使用goagent的local目录下的程序，修改proxy.ini使用php代理：

```
[php]
; 启动PHP代理
enable = 1
; 刚刚设置的密码
password = ******
crlf = 0
validate = 0
; 代理的端口号
listen = 127.0.0.1:8088
; 代理服务器地址，指定刚刚上传的index.php的路径
fetchserver = https://MYSITE/gae/index.php
```

保存后运行proxy.sh便算是启动了代理服务器。

使用byethost的php空间，感觉比GAE慢，不过只是备用，也不用太纠结，毕竟免费。



