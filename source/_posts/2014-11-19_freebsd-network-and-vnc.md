---
uuid:             f6558d0a-aebb-4517-b52e-7cd376605d73
layout:           post
title:            FreeBSD的网络配置和VNC使用
slug:             freebsd-network-and-vnc
subtitle:         null
date:             '2014-11-19T07:19:13.000Z'
updated:          '2015-07-29T22:42:33.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---



## 一，网络配置

### 1，开机启动配置

编辑/etc/rc.conf可以配置开机时使用的host，ip，gateway等，

```
# 配置HostName
hostname="ywxie"
# 配置IP和掩码
ifconfig_re0="inet 192.168.1.192 netmask 255.255.255.0"
# 配置默认网关
defaultrouter="192.168.1.200"
```

由于我想使用外部IP地址，而使用外部IP地址需要锐捷认证才能联网。那么问题就来了，开机启动加载各种模块，其中有些模块就需要用到网络，比如ntpdate和一些mail，如果启动这些的时候没有网络，便会超时，重启，然后整个重启过程便会很久很久，所以需要让锐捷认证放在它们前边，这就需要写rc.d脚本了，不想麻烦，于是就先使用内部IP启动相应的模块后，再设置外部IP，启动锐捷认证。

### 2，手动设置

```
# 手动配置IP
ifconfig re0 a.b.c.d/24
# 删除旧路由，增加新路由，对比Linux下的命令，少了一些参数
route delete default
route add default a.b.c.254
# 设置完成后，启动锐捷认证
# 锐捷没有出FreeBSD版本，给的Linux版本在Linux兼容层也运行不了，所以只能用mentohust代替
# 不过mentohust用起来，日志总是显示断开重新认证，不影响使用，不过毫无疑问加重了认证服务器的负担
mentohust
```

###  3, 使用openvpn客户端

FreeBSD使用openvpn，很简单，安装openvpn就行了：

    pkg install openvpn

安装完成后，下载需要的ovpn配置文件，使用ovpn配置文件启动openvpn客户端：

    openvpn --config vpngate_121.114.216.179_tcp_443.ovpn

连接成功后，访问网络的IP就变成了别的IP了。

一开始我是使用vpnbook的免费VPN和vcupboss的免费vpn，结果总是连接不上去，udp显示60s超时，握手不上，tcp显示连接超时，5s重连，然后瞎整了半天，最后使用vpngate给的免费VPN，终于连接成功。看来，前两者都被墙了，虽然手机用PPTP有时能连上。

## 二，使用VNC

VNC是很好用的远程桌面，我用的是tigervnc，既可以创建新的虚拟桌面，也可以使用现有的桌面，功能齐全。

### 1, 安装tigervnc

    pkg install tigervnc

### 2, 生成密码

为了一点点的 安全，这里设置使用密码来登录远程桌面，运行

    vncpasswd

这个命令会要求输入密码，然后生成密码的加密文件~/.vnc/passwd

### 3, 创建新的虚拟桌面

运行下述命令，创建一个800X600的小桌面，使用默认的密码文件~/.vnc/passwd，然后生成~/.vnc/xstartup文件

```
# 使用-geometry选项指定分辨率 
vncserver -geometry 800x600
```

新创建的小桌面什么都没启动，直接关了

    vncserver -kill :1

编辑~/.vnc/xstartup文件，增加启动项目，比如fcitx输入法，xfce4桌面等：

```
xsetroot -solid grey
# 生成的文件下边的代码都删掉，加入自己使用的，我这里加入了输入法和xfce4
/usr/local/bin/fcitx &
/usr/local/bin/xfce4-session &
```

编辑后，运行下述命令，然后输入密码，便可以看到xfce4的桌面：

    vncviewer localhost:1

### 4, 使用现有桌面

tigervnc提供了x0vncserver程序来使用现有桌面，这个我就使用得多了，往往工作中，启动了很多软件，然后吃个饭不想回来了，于是就ssh开启x0vncserver，然后再远程桌面连接，将各个软件和谐地退出，然后关机。

命令如下，这里需要手动指定密码文件的位置，否则总是提示没有设置密码，不给连接：

```
# 使用-PasswordFile来指定密码文件
x0vncserver -display :0 -PasswordFile=/home/padicao/.vnc/passwd
```

而连接远程桌面通用使用这个命令，不过localhost的连接，会有镜中镜的效果：

    vncviewer localhost:0

#### 4.1 使用x11vnc

在一开始我还不知道tigervnc提供了x0vncserver，然后就使用了在Linux下常用的x11vnc，这个命令跟x0vncserver类似，在一次意外中，运行下述命令，发现tigervnc提供了x0vncserver，man一下，发现跟x11vnc功能一致，于是就不再用x11vnc，少装一个重复功能的软件。

```
# 列出安装的tigervnc安装了哪些文件
# 这个命令很好用，很常用，可以看到安装了哪些文件，安装到哪些目录
pkg info -l tigervnc
```

x11vnc为Real桌面创建远程桌面连接的命令，跟tigervnc也很相似，首先，创建密码文件，同样保存在~/.vnc/passwd（也不知道两者的密码文件能共用不）：

    x11vnc -storepasswd

启动远程桌面连接：

```
# -forever表示客户端断开连接后，不关闭远程桌面，默认是关闭的
# -shared表示允许多个客户端连接，默认是不允许的
# -ncache缓存，不懂，先这样设，缓存总是有用的
x11vnc -ncache 10 -forever -shared -rfbauth ~/.vnc/passwd
```

vncviewer的连接命令一样，就不多说了。



