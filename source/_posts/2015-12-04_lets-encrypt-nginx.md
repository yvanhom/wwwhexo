---
uuid:             67df8a36-96e1-493a-b140-45b280d94a7b
layout:           post
title:            '在 nginx 中使用 Let''s Encrypt'
slug:             lets-encrypt-nginx
subtitle:         null
date:             '2015-12-04T05:56:20.000Z'
updated:          '2015-12-23T22:51:34.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

[Let's Encrypt](https://letsencrypt.org/) 已经开始公测，忍不住试用了一把，在我申请的 AWS VPS 上。

使用也是非常方便。

首先下载 Let's Encrypt:

```sh
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
```

然后生成证书，对于 nginx，我直接使用 webroot 的方式，像 apache 的方式明显是给 apache 的，standalone 的方式，我不清楚，webroot 的方式只需要指定 web 根目录，这个容易，我就先尝试使用这个。

```sh
./letsencrypt-auto certonly --webroot --webroot-path=/home/wwwroot/default/ -d aws.yvanhom.com
```

生成的证书文件保存在 `/etc/letsencrypt/live/aws.yvanhom.com/` 中，有这些文件

```
cert.pem  chain.pem  fullchain.pem  privkey.pem
```

之后配置 nginx 使用刚刚生成的证书，编辑 nginx.conf 文件，我使用 [lnmp 一键脚本](http://lnmp.org/)，文件在 /usr/local/nginx/conf 目录下，在 server 项增加三行配置

```
server
    {
        listen 80 default_server;
        listen 443 ssl;      # 从这里开始的三行
        ssl_certificate /etc/letsencrypt/live/aws.yvanhom.com/cert.pem;
        ssl_certificate_key /etc/letsencrypt/live/aws.yvanhom.com/privkey.pem;
        # 其他配置
    }
```

完成后重启 nginx 就搞定了。

```
service nginx restart
```

需要注意的是证书有效期三个月，过期需要重新运行命令。

**2015/12/24 补充**

webroot 的原理是将验证文件放置到网站根目录下，然后验证服务器访问域名下的验证文件，证实使用者拥有该域名，从而达到安全认证的目的。

至于 standalone，使用前需要将 http 服务器，比如 Apache 和 Nginx 关闭，猜测，standalone 的方式跟 webroot 的方式差不多，只不过 webroot 使用系统已经存在的 http 服务器，而 standalone 会自行启动一个自带的简单的 http 服务器接受验证服务器的访问验证。

使用 Nginx 代理 Ghost，这个时候需要再开辟一个目录，供 webroot 方式认证。webroot 方式使用到的目录是 .well-known。

以下是配置 Nginx，从而能够使用 webroot 方式获取 Let's Encrypt 的证书：

```
        # Ghost 代理
        location / {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $http_post;
            proxy_pass http://127.0.0.1:2586;
        }
        # 让 .well-known 目录不走代理
        location /.well-known {
            alias /home/wwwroot/ghost.yvanhom.com/.well-known;
            expires max;
        }
```
