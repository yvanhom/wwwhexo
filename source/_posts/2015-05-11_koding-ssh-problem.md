---
uuid:             f913fd20-8991-4d90-aa94-ffbb9e234179
layout:           post
title:            'Koding 问题：ssh 报错 Permission denied (publickey)'
slug:             koding-ssh-problem
subtitle:         null
date:             '2015-05-11T07:56:37.000Z'
updated:          '2015-07-29T22:34:18.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


Koding 用得好好的，吃个饭回来，自动关机，重启，然后 ssh 连接报错：

```
Offending ECDSA key in /home/xxxx/.ssh/known_hosts:39
ECDSA host key for xxxx.koding.io has changed and you have requested strict checking.
Host key verification failed.
```

见到这个错误，当然是顺手把 ~/.ssh/known_hosts 中的 koding 行删除，于是错误变成了这个：

`Permission denied (publickey).`

简单粗暴的错误提示，完全不知道怎么办，那么便看看服务器端的错误吧。

运行 `tail /var/log/auth.log` 可以看到：

`error: Could not load host key: /etc/ssh/ssh_host_ed25519_key`

查看了一下，没有这个文件，那么让 sshd 怎么加载（这个文件是怎么丢失的？）。

于是生成一下这个文件：

`ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ED25519_key`

然后重新 ssh 连接，又是 host key 改变，删除 known_hosts 中的特定行，重试。

搞定。

**补充**

第二天又不能连接上去，抽风啊。

把 /etc/ssh/ssh_host_ED25519_key 删除掉又能连接了。

诡异，算了，先升级再说。



