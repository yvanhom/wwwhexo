---
uuid:             e3ad71eb-71fa-455f-95c6-f31a23b61527
layout:           post
title:            无奈，只能各种翻墙
slug:             climb-wall
subtitle:         null
date:             '2014-12-23T08:05:33.000Z'
updated:          '2015-07-29T22:41:41.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - freebsd

---


最近入手Lumia，用到很多微软的服务，比如outlook，live，onenote，word online，onedrive等。

这些服务都能在手机上正常使用，可是一换到PC上，就各种被墙，真心麻烦，只能想方设法翻墙。

Android的谷歌被墙，关系不大，Lumia的微软被墙，多终端平台使用，就大大受限，非翻不可。


## 1, DNSCrypt

听网上说，onedrive等被墙的方式是DNS污染，使用DNS机密，也就是DNSCrypt便能解决，于是下载使用，能不用代理就不用代理。

在Win7上比较简单，直接安装便能够使用，只是有时候能开机启动，有时候不能。

在FreeBSD上，需要root权限，比较麻烦，root用户，安装dnscrypt-proxy：

    pkg install dnscrypt-proxy

root用户，启动该服务：

    service dnscrypt-proxy onestatus

由于安装在/usr/local下，不能直接写/etc/rc.conf直接开机启动，也懒得整了。

root用户，编辑/etc/resolv.conf文件，设置nameserver为127.0.0.1，文件内容如下：

    nameserver 127.0.0.1

使用dnscrypt后，当天可以使用。


## 2, 代理

整完DNSCrypt的第二天，onedrive就一直加载不成功。

用Firefox的 开发者 – 网络 ，查看加载过程，发现网站p.sfx.ms的一些文件都传输不了，看来已经不止是DNS污染的问题了，不得不用代理了。

可以使用snova over openshift，后来不知道在哪看到openshift禁止搭建代理，于是就不用了，把这个给删了。

也可以使用GoAgent over GAE，这个真心好用，不过老版本可能不能用，需要不断地更新。

使用代理顺利使用onedrive。

还没等我用个爽快，几天过后又不能用了，再次查看加载过程，这回是skyapi.onedrive.live.com的一直传输不了，又墙了一个，只能再添加新规则。

只能说订阅的GFWList的更新速度比不上被墙的速度。

我就不明白了，微软的这个服务怎么需要被墙。


## 3, 免费VPN

有时候真心需要使用全局的方式来翻墙。

可以为所有URL使用代理。

也可以使用VPN。

免费的VPN，我用过的有VPNBook的，只能说有时能连上，经常连不上，免费VPN不容易。

现在在用的是vpngate，有很多vpn可选择，有很多连不上，很多连上很快掉线，有些连上不能上网，不过总有些能连上，速度还不错。

也考虑使用付费VPN，可是觉得不划算，现在的使用量，用免费VPN和代理已经能够解决需求了。


## 4, 期望

哪里有压迫，哪里就有反抗。

哪里有墙，哪里就有翻墙。

希望有一天国内环境能够开放一些，使得国外的一些优质服务能够造福国内。



