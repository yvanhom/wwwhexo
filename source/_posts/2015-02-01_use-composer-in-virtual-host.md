---
uuid:             c4142cf7-0f95-4356-9dde-0d87d4b0f576
layout:           post
title:            在虚拟主机上部署使用Composer的PHP应用
slug:             use-composer-in-virtual-host
subtitle:         null
date:             '2015-02-01T02:56:46.000Z'
updated:          '2015-07-29T22:38:27.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


2014年Top 10的PHP框架中，Laravel居首位（见[这里](http://www.oschina.net/news/47752/top-10-php-frameworks-for-2014)）。

从这也可以看出，使用Laravel的PHP项目很多，而Laravel使用Composer，于是，使用Composer的项目更多。

更麻烦的是，很多PHP应用（比如[wardrobe](https://github.com/wardrobecms/wardrobe)，[gruik](https://github.com/grena/gruik)，[opennote](https://github.com/FoxUSA/OpenNoteService-PHP)，[paperwork](https://github.com/twostairs/paperwork)等）都是只提供源码包，没有把各种依赖加进去，于是下下来后需要运行下列命令来安装依赖包：

    composer install

而在虚拟主机上，很多是没有给你ssh功能的，就算是提供ssh功能的OpenShift也有严格的内存限制，我在OpenShift上安装wardrobe时，便提示内存超过限制而异常退出。

解决方案很简单，便是在自己的机器上安装PHP，composer等一堆东西。

虽然很简单，但是我可不想安装这些，因为安装那么多东西只用那么一两次，总觉得没兴致，于是就找了[koding.io](www.koding.io)提供的免费虚拟机。

koding.io提供的免费方案提供一个几乎是全权限（Ubuntu，可以sudo提权，可以安装各种软件包）的虚拟机，不止可以网页端ssh链接（会卡），还可以非网页的ssh链接（不卡，ping值只有50+ms），太完美了，不过美中不足的是，每个虚拟机在网页上一个小时没操作，便自动关机。于是经常我非网页的ssh操作得正得心应手，就给我关机了。虽然如此，作为composer编译，便快速测试是否成功，也是非常有用的。

注册，登陆就不说了。

### 设置远程ssh链接

本地：

```
# 生成id_rsa和id_rsa.pub文件
ssh-keygen 
 
# 将id_rsa放置到~/.ssh/下，如果没有这个目录，创建一个
mv id_rsa ~/.ssh/
```

koding.io虚拟机上（在网页上的ssh内操作）：

```
#没有~/.ssh的话，也创建一个 
cat id_rsq.pub >> ~/.ssh/authorized_keys
```

### 安装需要的软件包

```
 # 安装mysql，如果没有kpm，则根据提示安装kpm，而且可以直接使用kpm安装wordpress
curl -sSL "https://raw.githubusercontent.com/koding/kpm-scripts/master/kpm/installer" | sudo fish
 
# 安装mysql
sudo kpm install mysql
 
#安装phpmyadmin方便管理
sudo kpm install phpmyadmin
 
# 安装sqlite数据库
sudo apt-get install php5-sqlite
 
# 安装composer
sudo kpm install composer
```

### 部署composer应用

以wardrobe为例，wardrobe是一个轻量级的博客系统，喜欢它的轻量级，使用Markdown语法，简单，直观，无障碍写作。

```
# 1, 下载源码包，并解压缩
cd ~/Web
wget https://github.com/wardrobecms/wardrobe/archive/v1.1.0.zip
unzip v1.1.0.zip
# 换个简短的名字
mv wardrobe-1.1.0/ wardrobe
cd wardrobe
 
# 2, 安装依赖
composer install
 
# 3, 打包
zip -r ~/Web/wardrobe.zip ~/Web/wardrobe
 
# 4, 将需要的目录设置为可读
chmod -R a+w app/config/
chmod -R a+w app/database/      # 使用sqlite数据库时需要
chmod -R a+w app/storage/
chmod -R a+w public/
 
# 5, 编辑 app/config/database.php 设置数据库
vim app/config/database.php
# 'default' => 'mysql',
#                 'mysql' => array(
#                        'driver'    => 'mysql',
#                        'host'      => 'localhost',
#                        'database'  => 'wardrobe',
#                        'username'  => 'wardrobe',
#                        'password'  => 'xxxxxxxx',
#                        'charset'   => 'utf8',
#                        'collation' => 'utf8_unicode_ci',
#                        'prefix'    => 'wr_',
#                ),
 
# 6, 设置URL rewrite，将请求重定位到wardrobe/public下
vim ~/Web/.htaccess
# 
#    RewriteEngine on
#    # 将所有不是以wardrobe/public的网页请求重写URL
#    RewriteCond %{REQUEST_URI} !^wardrobe/public
#    # 补上需要的前缀wardrobe/public
#    RewriteRule ^(.*)$ wardrobe/public/$1 [L]
# 
# 7, wardrobe有bug，修复一下
cd ~/Web/public
ln -s packages/wardrobe/core/themes ./
```

到这里，便可以进入 koding.io给的域名网站，进行应用相关的安装。如果顺利，便说明可以了。

顺利后，利用上边第三步所打包后的文件，放置到虚拟主机上，解压，跳过第4步（可选），然后进入第5，6步。

### 总结

其实并不复杂，一句话来说，便是先在koding.io上将依赖关系处理好，然后打包放置到虚拟主机上。



