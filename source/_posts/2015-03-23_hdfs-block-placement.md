---
uuid:             d813d6d4-6aa7-4bc0-ab15-33f8ad5504be
layout:           post
title:            HDFS块布局分析
slug:             hdfs-block-placement
subtitle:         null
date:             '2015-03-23T05:48:54.000Z'
updated:          '2015-07-29T22:37:22.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - java

---


从网上相关描述，可以知道HDFS的三个副本的布局方式是：

- 本地，或本机架A的某个节点
- 另一个机架B的某个节点
- 机架B的另一个节点

这部分的代码主要在org/apache/hadoop/hdfs/server/blockmanagement中，使用BlockPlacementPolicy接口的多个chooseTarget来获取三个副本的位置。

在BlockPlacementPolicy中有一个getInstance的静态函数用于生成示例：

```
  public static BlockPlacementPolicy getInstance(Configuration conf, 
                                                 FSClusterStats stats,
                                                 NetworkTopology clusterMap) {
    Class<? extends BlockPlacementPolicy> replicatorClass =
                      conf.getClass("dfs.block.replicator.classname",
                                    BlockPlacementPolicyDefault.class,
                                    BlockPlacementPolicy.class);
    BlockPlacementPolicy replicator = (BlockPlacementPolicy) ReflectionUtils.newInstance(
                                                             replicatorClass, conf);
    replicator.initialize(conf, stats, clusterMap);
    return replicator;
  }
```

从中可以看出，用户可以通过设置dfs.block.replicator.classname来指定使用某种放置策略，可以是HDFS自带的BlockPlacementPolicyDefault，BlockPlacementPolicyWithNodeGroup，或者自己实现新的放置策略，当没有设置时，使用BlockPlacementPolicyDefault，而BlockPlacementPolicyDefault便使用了上述的放置方法。

BlockPlacementPolicyDefault比较复杂，考虑了favorite的节点，排除节点等，在各个chooseTarget里跳来跳去，这里略过这一部分，各个chooseTarget最终会进入一个私有的chooseTarget：

```
  private DatanodeDescriptor chooseTarget(int numOfReplicas,
                                          DatanodeDescriptor writer,
                                          ......) {
    ......
    # numOfResults表示已经选择了多少个节点
    try {
      if (numOfResults == 0) {
        // 第一步，选择本节点或本机架的某个节点
        writer = chooseLocalNode(writer, excludedNodes, blocksize,
            maxNodesPerRack, results, avoidStaleNodes);
        if (--numOfReplicas == 0) {
          return writer;
        }
      }
      if (numOfResults <= 1) {
        // 第二步，选择远程机架的某个节点
        chooseRemoteRack(1, results.get(0), excludedNodes, blocksize,
            maxNodesPerRack, results, avoidStaleNodes);
        if (--numOfReplicas == 0) {
          return writer;
        }
      }
      if (numOfResults <= 2) {
        // 第三步，选择远程机架的又一节点
        if (clusterMap.isOnSameRack(results.get(0), results.get(1))) {
          // 如果前边两个节点在同一机架，那么便选远程机架的某个节点
          chooseRemoteRack(1, results.get(0), excludedNodes,
                           blocksize, maxNodesPerRack, 
                           results, avoidStaleNodes);
        } else if (newBlock){
          // 跟第二个节点同机架的节点
          chooseLocalRack(results.get(1), excludedNodes, blocksize, 
                          maxNodesPerRack, results, avoidStaleNodes);
        } else {
          chooseLocalRack(writer, excludedNodes, blocksize, maxNodesPerRack,
              results, avoidStaleNodes);
        }
        if (--numOfReplicas == 0) {
          return writer;
        }
      }
      // 剩下的节点随机选择
      chooseRandom(numOfReplicas, NodeBase.ROOT, excludedNodes, blocksize,
          maxNodesPerRack, results, avoidStaleNodes);
    } catch (NotEnoughReplicasException e) {
      LOG.warn("Not able to place enough replicas, still in need of "
               + (totalReplicasExpected - results.size()) + " to reach "
               + totalReplicasExpected + "n"
               + e.getMessage());
      if (avoidStaleNodes) {
        // Retry chooseTarget again, this time not avoiding stale nodes.
 
        // excludedNodes contains the initial excludedNodes and nodes that were
        // not chosen because they were stale, decommissioned, etc.
        // We need to additionally exclude the nodes that were added to the 
        // result list in the successful calls to choose*() above.
        for (Node node : results) {
          oldExcludedNodes.put(node, node);
        }
        // Set numOfReplicas, since it can get out of sync with the result list
        // if the NotEnoughReplicasException was thrown in chooseRandom().
        numOfReplicas = totalReplicasExpected - results.size();
        return chooseTarget(numOfReplicas, writer, oldExcludedNodes, blocksize,
            maxNodesPerRack, results, false);
      }
    }
    return writer;
  }
```

选择后，回到公共的chooseTarget，对多个节点进行流水线布置：

`return getPipeline(...);`



