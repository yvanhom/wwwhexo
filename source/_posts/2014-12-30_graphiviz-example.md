---
uuid:             5c5156be-13aa-479a-8a43-cf90b0291e2e
layout:           post
title:            使用graphviz自动生成结构图
slug:             graphiviz-example
subtitle:         null
date:             '2014-12-30T20:51:04.000Z'
updated:          '2016-07-04T08:40:46.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - java
  - linux
  - freebsd
  - shell

---


graphviz使用dot语言描述结构，并自动生成所需要的结构图。

跟Latex一样，是“所想即所得”，区别于一般画图工具的“所见即所得”。

dot语言简明易懂，具体可以见[这个博客](http://www.cnblogs.com/sld666666/archive/2010/06/25/1765510.html "dot语言实例")的几个实例。

使用dot语言，好处便是能在程序中自动输出描述结构的文本，然后编译出图形出来。

下面给出的是使用Java语言，将一个网络拓扑图使用dot语言描述。（程序只是一部分）

```
    public String getTopoStatsAsDot() {
        StringBuilder sb = new StringBuilder();
        sb.append("digraph topo {n");
        Collection<NSEntry> values = topoStats.values();
        for (NSEntry ent : values) {
            if (ent.id.type == EntryType.HOST) {
                sb.append(String.format("  h_%d[shape=box,style=filled,color=blue,label="%s"]n",
                        ent.id.id, ent.dev.getMACAddressString()));
            } else {
                sb.append(String.format("  sw_%d[shape=box,style=filled,color=green]n",
                        ent.id.id));
            }
 
        }
        for (NSLinkStat ls : allLinkStats) {
            String n1, n2;
            if (ls.a.id.type == EntryType.HOST) {
                n1 = String.format("h_%d", ls.a.id.id);
            } else {
                n1 = String.format("sw_%d", ls.a.id.id);
            }
 
            if (ls.b.id.type == EntryType.HOST) {
                n2 = String.format("h_%d", ls.b.id.id);
            } else {
                n2 = String.format("sw_%d", ls.b.id.id);
            }
            sb.append(String.format("  %s -> %s[label="%d"]n", n1, n2,
                    ls.a2bCurAll - ls.a2bLastAll));
            sb.append(String.format("  %s -> %s[label="%d"]n", n2, n1,
                    ls.b2aCurAll - ls.b2aLastAll));
        }
 
        sb.append("}n");
        return sb.toString();
    }
```

下面给出的是自动生成的dot语言源文件：

```
digraph topo {
  h_3[shape=box,style=filled,color=blue,label="00:00:00:00:00:04"]
  sw_2[shape=box,style=filled,color=green]
  sw_3[shape=box,style=filled,color=green]
  h_2[shape=box,style=filled,color=blue,label="00:00:00:00:00:02"]
  h_1[shape=box,style=filled,color=blue,label="00:00:00:00:00:01"]
  h_0[shape=box,style=filled,color=blue,label="00:00:00:00:00:03"]
  sw_1[shape=box,style=filled,color=green]
  sw_1 -> sw_2[label="98144855"]
  sw_2 -> sw_1[label="330201"]
  sw_1 -> sw_3[label="330201"]
  sw_3 -> sw_1[label="98144855"]
  sw_2 -> h_1[label="98144924"]
  h_1 -> sw_2[label="330140"]
  sw_3 -> h_0[label="330270"]
  h_0 -> sw_3[label="98165132"]
  sw_2 -> h_2[label="130"]
  h_2 -> sw_2[label="0"]
  sw_3 -> h_3[label="130"]
  h_3 -> sw_3[label="0"]
}
```

运行以下命令生成PNG图像：

    dot topo.dot -Tpng -o topo.png

以下是生成的结果图：

![使用graphviz生成的网络拓扑图](http://o9sak9o6o.bkt.clouddn.com/techs/graphivz.png)

