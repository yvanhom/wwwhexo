---
uuid:             70a22eb5-b402-4efa-878a-4760a6084262
layout:           post
title:            '尝试 loop 设备与块设备加密 CryptSetup'
slug:             loop-and-cryptsetup
subtitle:         null
date:             '2016-11-03T15:39:02.000Z'
updated:          '2016-11-04T03:43:42.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---

最近检查了下电脑，发现了不少乱七八糟的数据，得加密保存一下。于是就稍微尝试尝试。

### EncFS

EncFS是一个免费的，开源的，基于GPL的，FUSE级别的加密文件系统，能够透明加密文件，使用任意目录存放加密文件。

优点是易用，易扩充，不需 root 权限就能使用。

缺点是每个目录、文件都在加密目录下有对应的目录、文件，挺乱的。

```sh
# 创建 / 挂载
## 创建时（首次运行），输入 p 进入预设的极端模式，然后输入两次密码，完成创建，创建后自动挂载
# ~/encrypted 是加密目录，~/decrypted 是解密目录，不能使用相对路径
encfs ~/encrypted ~/decrypted

# 卸载
fusermount -u ~/decrypted
```

### CryptSetup

CryptSetup 是 linux 下的一个分区加密工具，比 EncFS 面向的文件系统级别工作在更底层的位置。

优点：块加密，更安全，更快。

缺点：需要 root 权限。

```sh
# 1. 创建 loop 设备
## 创建一个 1G 的大文件
dd if=/dev/zero of=./image.img bs=1M count=1024
## 映射为 loop 设备
sudo losetup /dev/loop0 ./image.img
## 解除映射使用 sudo losetup -d /dev/loop0

# 2. cryptsetup
## 选择1：使用 plain dm-crypt
## 创建 / 加载，这里需要输入密码
sudo cryptsetup -y create secretfs /dev/loop0
## 卸载使用 sudo cryptsetup remove secretfs

## 选择2：使用 LUKS
## 创建。首次运行时需要格式化，有提示，需要输入**大写的** YES。需要输入密码。
sudo cryptsetup -y luksFormat /dev/loop0
## 加载，需要输入密码
sudo cryptsetup luksOpen /dev/loop0 secretfs
## 卸载使用 sudo cryptsetup luksClose secretfs

## 查看 secretfs 状态
sudo cryptsetup status secretfs

# 3. 文件系统挂载
## 格式化。首次运行时需要。
sudo mke2fs /dev/mapper/secretfs
## 挂载
sudo mount /dev/mapper/secretfs ./decrypted
## 权限交给普通用户。首次运行时需要。
sudo chown user:user ./decrypted

# 4. 结束使用
sudo umount ./decrypted
sudo cryptsetup remove secretfs
## OR sudo cryptsetup luksClose secretfs
sudo losetup -d /dev/loop0
```

