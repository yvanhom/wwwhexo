---
uuid:             31c91327-8843-4950-acc0-37250a9445f4
layout:           post
title:            '使用 awk 的字符串分割函数'
slug:             awk-split-string
subtitle:         null
date:             '2015-11-05T22:48:26.000Z'
updated:          '2015-11-05T22:48:26.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---

处理日志时，经常遇到时间字符串，比如 `05:28:50.275`，分割字符串的操作方法很多，这里实例介绍下 awk 的 split 函数。

下边是 redis 运行时的一段日志：

```
[23639] 05 Nov 05:25:39.575 # Server started, Redis version 2.8.22
[23639] 05 Nov 05:25:39.575 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
[23639] 05 Nov 05:25:39.575 * DB loaded from disk: 0.000 seconds
[23639] 05 Nov 05:25:39.575 * The server is now ready to accept connections on port 6379
[23639] 05 Nov 05:28:50.275 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:28:50.275 * Background saving started by pid 23693
[23693] 05 Nov 05:28:50.389 * DB saved on disk
[23693] 05 Nov 05:28:50.389 * RDB: 3 MB of memory used by copy-on-write
[23639] 05 Nov 05:28:50.475 * Background saving terminated with success
[23639] 05 Nov 05:29:51.085 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:29:51.101 * Background saving started by pid 23695
[23695] 05 Nov 05:29:59.876 * DB saved on disk
[23695] 05 Nov 05:29:59.885 * RDB: 164 MB of memory used by copy-on-write
[23639] 05 Nov 05:30:00.001 * Background saving terminated with success
[23639] 05 Nov 05:31:01.051 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:31:01.081 * Background saving started by pid 23733
[23733] 05 Nov 05:31:20.117 * DB saved on disk
[23733] 05 Nov 05:31:20.142 * RDB: 355 MB of memory used by copy-on-write
[23639] 05 Nov 05:31:20.281 * Background saving terminated with success
[23639] 05 Nov 05:32:21.081 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:32:21.126 * Background saving started by pid 23739
[23739] 05 Nov 05:32:44.537 * DB saved on disk
[23739] 05 Nov 05:32:44.577 * RDB: 0 MB of memory used by copy-on-write
[23639] 05 Nov 05:32:44.726 * Background saving terminated with success
[23639] 05 Nov 05:35:38.026 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:35:38.070 * Background saving started by pid 23783
[23783] 05 Nov 05:36:02.948 * DB saved on disk
[23783] 05 Nov 05:36:02.981 * RDB: 1116 MB of memory used by copy-on-write
[23639] 05 Nov 05:36:03.170 * Background saving terminated with success
[23639] 05 Nov 05:37:04.070 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:37:04.117 * Background saving started by pid 23818
[23818] 05 Nov 05:37:28.371 * DB saved on disk
[23818] 05 Nov 05:37:28.421 * RDB: 169 MB of memory used by copy-on-write
[23639] 05 Nov 05:37:28.517 * Background saving terminated with success
[23639] 05 Nov 05:38:29.017 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:38:29.067 * Background saving started by pid 23823
[23823] 05 Nov 05:38:51.198 * DB saved on disk
[23823] 05 Nov 05:38:51.227 * RDB: 104 MB of memory used by copy-on-write
[23639] 05 Nov 05:38:51.367 * Background saving terminated with success
[23639] 05 Nov 05:40:02.167 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:40:02.215 * Background saving started by pid 23894
[23894] 05 Nov 05:40:27.261 * DB saved on disk
[23894] 05 Nov 05:40:27.313 * RDB: 172 MB of memory used by copy-on-write
[23639] 05 Nov 05:40:27.415 * Background saving terminated with success
[23639] 05 Nov 05:41:28.031 * 10000 changes in 60 seconds. Saving...
[23639] 05 Nov 05:41:28.082 * Background saving started by pid 23929
[23929] 05 Nov 05:41:51.285 * DB saved on disk
[23929] 05 Nov 05:41:51.316 * RDB: 107 MB of memory used by copy-on-write
[23639] 05 Nov 05:41:51.382 * Background saving terminated with success
[23639] 05 Nov 05:46:52.097 * 10 changes in 300 seconds. Saving...
[23639] 05 Nov 05:46:52.152 * Background saving started by pid 23939
[23939] 05 Nov 05:47:15.610 * DB saved on disk
[23939] 05 Nov 05:47:15.665 * RDB: 12 MB of memory used by copy-on-write
[23639] 05 Nov 05:47:15.752 * Background saving terminated with success
^C[23639 | signal handler] (1446673670) Received SIGINT scheduling shutdown...
[23639] 05 Nov 05:47:50.352 # User requested shutdown...
[23639] 05 Nov 05:47:50.352 * Saving the final RDB snapshot before exiting.
[23639] 05 Nov 05:48:13.566 * DB saved on disk
[23639] 05 Nov 05:48:13.566 # Redis is now ready to exit, bye bye...
```

对这份日志进行分析，获取 redis 的持久化时间。

下边是 awk 代码：

```awk
{
        if($8 == "started") {
                starttime = $4
        }else if($8 == "terminated"){
                endtime = $4
                #printf("%s - %s\n", starttime, endtime)
                split(starttime, a, ":")
                st = a[1] * 3600 + a[2] * 60 + a[3]
                split(endtime, b, ":")
                et = b[1] * 3600 + b[2] * 60 + b[3]
                printf("duration: %.2f\n", et - st)
        }
}
```

简要分析一下，首先通过关键字获取持久化开始时间和结束时间，

```awk
        if($8 == "started") {
                starttime = $4
        }else if($8 == "terminated"){
                endtime = $4
```

然后通过 split 函数获得时分秒，并转换成秒数

```awk
                split(starttime, a, ":")
                st = a[1] * 3600 + a[2] * 60 + a[3]
                split(endtime, b, ":")
                et = b[1] * 3600 + b[2] * 60 + b[3]
```

最后，计算持续时间

```awk
                printf("duration: %.2f\n", et - st)
```

这是上边日志的分析结果

```
duration: 0.20
duration: 8.90
duration: 19.20
duration: 23.60
duration: 25.10
duration: 24.40
duration: 22.30
duration: 25.20
duration: 23.30
duration: 23.60
```

可以看到，刚启动时数据量少，很快便完成持久化操作，运行一段时间后，数据量增加，操作时间增加。一段时间后，数据量不再增加，持久化操作时间趋于稳定。


