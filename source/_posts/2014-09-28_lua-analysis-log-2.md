---
uuid:             d3f29cd7-1f5d-4a71-a2fe-4bf0eeb2d55c
layout:           post
title:            使用lua来简单分析日志（二）
slug:             lua-analysis-log-2
subtitle:         null
date:             '2014-09-28T05:46:50.000Z'
updated:          '2015-07-29T22:44:31.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---



## 一，情景分析

有多份日志，每份日志会输出多个带宽信息，需要把多份日志的第n个带宽信息加起来，并输出。

这里使用lua来分析，是因为涉及到多个文件的同时处理，使用shell的话会增加复杂度。


## 二，分析说明

首先是对单个文件进行分析，这里给通用for循环提供一个函数。（不得不感叹lua的强大，闭包功能的强大，for的强大）

```
function getBWFromFile(filename)
        file = io.open(filename)
        if file == nil then return nil end
 
        return function ()
                for line in file:lines() do
                        -- 当匹配到时，返回
                        bw = string.match(line, "throughput (%d+%.?%d+)")
                        if bw ~= nil then
                                return bw;
                        end
                end
        end
end
```

然后是多个文件的同时处理，照样使用for循环。

```
-- 结果数组
result = {}
 
-- 循环处理多个文件
for i = 2,4 do
        index = 1
        -- 处理某个文件
        for x in getBWFromFile(PREFIX .. "_vm177" .. i .. ".log") do
                -- 保存结果
                if result[index] == nil then
                        result[index] = 0
                end
                result[index] = result[index] + x
                index = index + 1
        end
end
```

最后是对结果进行输出，打印每个结果，并求平均值。

```
all = 0
for _, r in ipairs(result) do
        all = all + r
        print(r)
end
 
print("Average: " .. all / #result)
```

## 三，汇总

```
function getBWFromFile(filename)
        file = io.open(filename)
        if file == nil then return nil end
 
        return function ()
                for line in file:lines() do
                        bw = string.match(line, "throughput (%d+%.?%d+)")
                        if bw ~= nil then
                                return bw;
                        end
                end
        end
end
 
PREFIX = arg[1]
if PREFIX == nil then
        print("Usage: " .. arg[0] .. " FilePrefix")
end
 
result = {}
 
for i = 2,4 do
        index = 1
        for x in getBWFromFile(PREFIX .. "_vm177" .. i .. ".log") do
                if result[index] == nil then
                        result[index] = 0
                end
                result[index] = result[index] + x
                index = index + 1
        end
end
 
all = 0
for _, r in ipairs(result) do
        all = all + r
        print(r)
end
 
print("Average: " .. all / #result)
```



