---
uuid:             209c2a61-90d1-423c-a24c-18d70f6c4a43
layout:           post
title:            '学习 Rust 语言 -- FreeBSD 终于也能用 Cargo 了'
slug:             freebsd-rust
subtitle:         null
date:             '2015-07-22T17:04:21.000Z'
updated:          '2015-07-29T22:31:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd
  - rust

---


[之前的文章](https://www.yvanhom.com/2015/05/11/%E5%AD%A6%E4%B9%A0-rust-%E8%AF%AD%E8%A8%80-%E6%90%AD%E5%BB%BA%E7%8E%AF%E5%A2%83/)中提到，在 FreeBSD 中使用 Cargo 失败，使用[网络上的这个方法](https://csperkins.org/research/misc/2015-01-02-cargo-freebsd.html)，没能成功编译出 Cargo，心灰意冷之下只能远程编程。

不想今天 google 了一下，找到了[新的方法](https://www.freebsdnews.com/2015/07/17/rust-cargo-freebsd/)，7 月份的文章，实在是太有用了。

这里把步骤简单地说明一下：

- 添加 pkg 库：root 下新建文件 /etc/pkg/cargo.conf，这是私人构建的库，相信他们

```
Cargo: {
    url: "pkg+https://s3-us-west-1.amazonaws.com/freebsd-repos/edenbsd",
    mirror_type: "srv",
    signature_type: "pubkey",
    pubkey: "https://s3-us-west-1.amazonaws.com/freebsd-repos/eden-poudriere.pub",
    enabled: yes
}
```

- 安装 Cargo：这个过程出现了很多跟 pkg 库相关的错误警告，都被我忽略了，最后安装成功

```sh
pkg update
pkg install cargo
```

- 把新的 pkg 库删掉，不然每次都会有很多的错误警告

```sh
mv /etc/pkg/cargo.conf /etc/pkg/.cargo.conf.backup
```


