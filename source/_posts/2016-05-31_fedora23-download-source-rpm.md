---
uuid:             389dd5c1-dede-4167-8999-caaccfe2689e
layout:           post
title:            网站彻底长草，稍微除一下
slug:             fedora23-download-source-rpm
subtitle:         null
date:             '2016-05-31T03:01:09.000Z'
updated:          '2016-05-31T03:02:07.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - shell

---

悲剧，都半年没更新了。

也是，感觉自己没有什么技术好分享的。网上的东西那么多，要出新东西太难了，要出别人还没出的东西太难了，自己写的也不一定最好，只是给搜索引擎增加负担而已，而且随着时间推移，写的东西难免过时，失效，于是只能造成别人的困扰。

所以一直在长草。

而这次稍微除草，也名不副实，只是想记录下一些命令而已，这样，以后有这些问题，就不用搜 Google 了，直接搜自己的网站就行了。

Fedora 23 中下载源码包，不需要 root 权限，下载到当前目录:

```sh
dnf download --source XXX
```

rpm 解包：

```sh
rpm2cpio XXX.rpm | cpio -div
```
好了，水完了。
