---
uuid:             b2048bb5-f118-4dd4-8ef9-e0c1aa473bff
layout:           post
title:            活用脚本，一键运行
slug:             one-script-to-run
subtitle:         null
date:             '2014-10-14T03:14:00.000Z'
updated:          '2015-07-29T22:43:50.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---



## 一，说明

有时候，某些测试，需要在多个PC上运行，这个时候，通过ssh连接到某个pc上，运行某条命令，然后在ssh到另一个pc上运行别的命令，总的来说，非常麻烦，这个时候可以使用ssh来简化这个过程。


## 二，例子

这里列举iperf测试来说明以下：

```
#!/bin/bash
 
# 一些变量
PSERVER=vm1
PCLIENT=vm2
PSLOG=server.log
PCLOG=client.log
TIME=10
 
# 在PSSERVER上运行iperf服务器
echo == start iperf server
ssh $PSERVER iperf -s > $PSLOG &
# 在PCLIENT上运行iperf客户端
echo == Start iperf Client
ssh $PCLIENT iperf -c $PSERVER -t 10000 > $PCLOG &
 
echo == wait
sleep $TIME
 
# 运行到时后关闭
echo == Stop iperf
ssh $PCLIENT pkill iperf
sleep 1
ssh $PSERVER pkill iperf
 
# 获取结果
echo == Get Result
tail -n 1 $PCLOG
```
 



