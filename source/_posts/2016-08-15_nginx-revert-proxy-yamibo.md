---
uuid:             e3c3b4ee-1dcd-4a10-a5df-99f4d3374c08
layout:           post
title:            'nginx 反代 yamibo'
slug:             nginx-revert-proxy-yamibo
subtitle:         null
date:             '2016-08-15T21:41:28.000Z'
updated:          '2016-08-15T21:42:00.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

[百合会](http://www.yamibo.com)的移动线路一直在抽风，用手机流量经常上不去，想上的时候上不去，那是多么哀伤的事情，所以就建了个反代，以备不时之需。

首先，使用 **lnmp** 建个虚拟主机：

```sh
sudo lnmp vhost add
```

根据提示输入域名，其他都不需要，可以全 *NO*。

之后，修改虚拟主机的配置文件（在 */usr/local/nginx/conf/vhost/* 下），修改后的配置文件入下：

```
server
    {
        listen 80;
        server_name yamibo.yvanhom.com;

        location ~
        {
            proxy_pass http://www.yamibo.com;

            proxy_set_header Host www.yamibo.com;
            proxy_set_header User-Agent $http_user_agent;
            proxy_set_header Referer http://www.yamibo.com;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Accept-Encoding "";

            subs_filter_types text/css text/xml text/javascripts;
            subs_filter www.yamibo.com yamibo.yvanhom.com;
        }
    }
```

其中比较重要的配置有：

* `proxy_pass` 配置要反代的服务器
* `proxy_set_header Host` 访问百合会，必须设置百合会的域名
* `proxy_set_header User-Agent` 设置这个，从而手机/PC/平板等可访问不同的网页
* `proxy_set_header Accept-Encoding` 设置压缩编码，关闭，从而使得后边的 `subs_filter` 能够运作
* `subs_filter` 字符串替换
* `subs_filter_types` 要执行字符串替换的数据流

由于是自用，且是为不时之需，就懒得配 `cache` 了。

