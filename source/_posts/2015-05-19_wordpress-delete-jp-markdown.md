---
uuid:             73221157-a782-4041-bd46-208584a0a2e9
layout:           post
title:            '卸载 JP Markdown 后......'
slug:             wordpress-delete-jp-markdown
subtitle:         null
date:             '2015-05-19T16:46:42.000Z'
updated:          '2015-07-29T22:33:46.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---


[JP Markdown](https://wordpress.org/plugins/jetpack-markdown/) 是 WordPress 的一个插件，是作者 [JP Bot](https://profiles.wordpress.org/wpjp/) 从 jetpack 抽出的单独的 Markdown 模块。通过这个模块，用户可以使用 Markdown 语法来书写博客。

使用这个模块近几个月了，出现了问题，发现作者已经打算今年结束后便不再维护这个插件，所以也就不拿这个问题去打扰作者了，直接不使用这个插件。

发现的问题是这个插件跟 WordPress 的可视化编辑器冲突，一旦点了可视化编辑器，那么格式便乱了。所以要么不使用这个插件，要么不使用可视化编辑器。后者可行，不过有一些功能还是使用可视化编辑器方便，比如 Crayon 的高亮代码，插入带有些许属性的图片等。

既然不打算使用这个插件，那么便卸载了便是。

可是这个插件导致我的博客文章，有些是全 Html 的，有些是一半 Markdown，一半 Html。由于 Markdown 兼容 Html，所以使用这个插件没问题，一旦不使用这个插件，过往地文章一编辑起来，便乱了，也就是说卸载后，编辑过往文章会变得很麻烦。

那么怎么办呢？

很直观地，便是把所有的博文都通过 markdown 转 Html 进行转换，于是我又用起了我熟悉的 Lua 脚本来操作。

- 首先，将所有博文导出成一个 xml 文件。
- 下载 [markdown.lua](http://luaforge.net/projects/markdown/)，这是一个 Lua 的 Markdown 转 Html 的库，虽然很久没更新，不过依然可用，这归功于 Markdown 官方语法很久没更新扩展了。
- 编写转换的 Lua 脚本，代码如下：

```
require "markdown" 

local file = io.open(arg[1]) -- 读取整个文件 
local text = file:read("*all") 
local index = 1 
while text ~= nil 
do 
-- 找到博文正文开始的标记 <content:encoded><[CDATA[，其中 [ 需要做转义 %[
    local i1,i2 = string.find(text, "<content:encoded><%[CDATA%[", index) 
    if i1 == nil then
        -- 输出剩余的部分 
        local last = string.sub(text, index) 
        io.write(last)
        break;
    end
    -- 输出博文前边的内容
    local pre = string.sub(text, index, i2)
    io.write(pre)
    -- 找到博文正文结束的标记 ]]></content:encoded>
    local i3,i4 = string.find(text, "%]%]></content:encoded>", i2)
    -- 取出正文，做 markdown to html 转换，并输出
    local content = string.sub(text, i2 + 1, i3 - 1) 
    io.write(markdown(content))
    index = i3
end
```

- 进行转换：

```
lua m2h.lua 1.xml > 2.xml
```

- 网站删除所有博文，然后导入转换完成的 xml 文件。

**注意**：原来的xml文件不要删除，做好备份，避免出问题导致数据丢失。



