---
uuid:             28ba4e99-a8ae-4239-ab8b-7f68b5edfb35
layout:           post
title:            安装和配置FreeBSD
slug:             freebsd-install-and-setup
subtitle:         null
date:             '2014-11-14T06:21:09.000Z'
updated:          '2015-07-29T22:42:43.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---



## 1, 从U盘安装

下载FreeBSD-*-RELEASE-*-memstick.img.xz，然后解压缩出img文件，并使用dd写入U盘，需要花费很多时间：

    sudo dd if=./FreeBSD*.img of=/dev/sdX

写入完成后，重启计算机，选择从U盘启动。

## 2, 安装系统

跟着安装提示信息一步一步走，就行了。

注意1：手动分区时，需要创建一个新的FreeBSD主分区，然后在这个主分区上新建两个子分区，一个挂载/，一个做swap。

注意2：默认的安装是不会安装MBR的，由于我的PC原来安装的是GRUB，而新装的FreeBSD会覆盖掉，导致GRUB无法使用，所以必须安装一个新的MBR，否则启动不了。安装完成后，键盘输入Alt + F4进入一个新的终端，运行

```
# /dev/ad6是我的硬盘 
boot0cfg -B /dev/ad6
```

##  3, 配置

###  3.1 本地源

FreeBSD 10 似乎使用的新的pkg，ports系统，导致原来的很多配置不能用，各种国内的镜像源也不能用。

不过幸运的是官方源速度还行， 在校园网下，还有200+KBytes/s的速度，基本满足需求，所以这一步就不做了。

### 3.2 ports工具

参看[FreeBSDChina wiki: FreeBSD 的 Ports 系统。](https://wiki.freebsdchina.org/faq/ports "FreeBSDChina wiki: FreeBSD 的 Ports 系统")

使用portsnap更新ports，运行

    portsnap fetch extract

安装包更新工具portmaster，包清理工具pkg_rmleaves，运行

    pkg install portmaster pkg_rmleaves

###  3.3 安装xfce4

给make.conf添加选项，使得通过ports安装的包都支持kms：

    WITH_KMS=yes
    WITH_NEW_XORG=yes

安装xorg-minimal，xfce4等，所有的桌面环境都以来xorg，这里最小化安装xorg：

    pkg install xorg-minimal xfce4

运行Xorg –configure生成xorg.conf.new文件，运行Cirl + Alt + Back回到终端，并将xorg.conf.new复制到/etc/X11/xorg.conf：

    Xorg --configure
    mv xorg.conf.new xorg.conf

配置~/.xinitrc，使得startx时启动xfce4：

    echo '/usr/local/bin/startxfce4' >> ~/.xinitrc

### 3.4 中文化

配置中文locale，添加以下信息到~/.login_conf末尾：

```
me:
    :lang=zh_CN.UTF-8:
    :setenv=LC_ALL=zh_CN.UTF-8:
    :setenv=LC_CTYPE=zh_CN.UTF-8:
    :setenv=LC_COLLATE=zh_CN.UTF-8:
    :setenv=LC_TIME=zh_CN.UTF-8:
    :setenv=LC_NUMERIC=zh_CN.UTF-8:
    :setenv=LC_MONETARY=zh_CN.UTF-8:
    :setenv=LC_MESSAGES=zh_CN.UTF-8:
    :charset=UTF-8:
    :xmodifiers="@im=fcitx"
```

安装wqy字体，安装fcitx：

    pkg install wqy-fonts zh-fcitx zh-fcitx-configtool

使用wqy字体，编辑/etx/X11/xorg.conf，编辑后如下，这里注释掉了一些不存在的字体目录，增加了wqy，dejavu目录：

```
Section "Files"
        ModulePath   "/usr/local/lib/xorg/modules"
        #FontPath     "/usr/local/lib/X11/fonts/misc/"
        FontPath     "/usr/local/lib/X11/fonts/TTF/"
        FontPath     "/usr/local/lib/X11/fonts/OTF/"
        FontPath     "/usr/local/lib/X11/fonts/dejavu/"
        FontPath     "/usr/local/lib/X11/fonts/wqy/"
        #FontPath     "/usr/local/lib/X11/fonts/Type1/"
        #FontPath     "/usr/local/lib/X11/fonts/100dpi/"
        #FontPath     "/usr/local/lib/X11/fonts/75dpi/"
EndSection
```

配置使用fcitx，编辑.xinitrc，编辑后内容如下：

    fcitx & 
    /usr/local/bin/startxfce4

以上编辑完成后，重启，运行startx进入xfce后，可以显示中文，输入中文。

注意：xfce4-terminal可能不能输入中文，测试发现，su后的root用户能够输入中文，普通用户bash后也能够输入中文，看来是普通用户的sh需要做些额外配置才能输入中文，不过这个太麻烦了，所以直接配置，使用bash，而不是sh。

运行chsh，进入vim编辑模式使用编辑Shell使用/usr/local/bin/bash

### 3.5 配置intel显卡

按照[这个流程](http://www.2cto.com/os/201402/280526.html "FreeBSD配置Intel显卡")走了一趟，然后发现startx能进xfce4，可是从xfce4退出后，就一直黑屏。


## 3.6 软件安装

浏览器：firefox，firefox-i18n

Office：libreoffice zh_CN-libreoffice

PDF阅读：evince

Java编程：NetBeans（汗，这里FreeBSD pkg把所有插件都安装了）

字典：

### 3.7 挂载其它文件系统

挂载ext2，ext3

    mount -t ext2fs /dev/ad5s1 /mnt

挂载ext4

```
pkg install fusefs-ext4fuse
kldload fuse
ext4fuse /dev/ad5s1 /mnt
```

挂载ntfs

```
pkg install fusefs-ntfs
# kldload fuse
ntfs-3g /dev/ad6s1 /mnt
```

###  3.8 声音

安装FreeBSD 10后，用了几天，一切正常，想要听点歌，发现vlc播放没有声音，于是就各种整。

首先加载通用的声音驱动模块

    kldload snd_driver

查看有没有识别到自家的声卡

    cat /dev/sndstat

我的显示如下：

```
Installed devices:
pcm0: <Realtek ALC887 (Rear Analog)> (play/rec)
pcm1: <Realtek ALC887 (Front Analog)> (play/rec) default
pcm2: <Realtek ALC887 (Onboard Digital)> (play)
```

很幸运的，我的声卡，默认的FreeBSD能支持，如果没有Installed device中没有，那么便可能需要编译FreeBSD的内核和模块，将自己的声卡驱动加进去，会很麻烦的。

可是依然没有声音。于是就各种尝试。

百度了下，发现我的声卡需要snd*hda，可是kldstat | grep snd*hda，却什么都没显示，然后手动加载一下kldload snd_hda，提示：

    kldload: can't load snd_hda: module already loaded or in kernel

也就是说声卡驱动已经编译到内核了。

当然我上边都是瞎整，/dev/sdnstat都显示了设备已安装，也就是说驱动已经没问题了，那么只能是别的地方出问题。

于是只能google，上freebsdchina论坛，然后发现了这个：

    sysctl hw.snd.default_unit=1

一运行，还真有用，声音出来了。

于是编辑/etc/sysctl.conf，在末尾加入一行，将这个设置变成开机启动

    hw.snd.default_unit=1

这个设置对照前边的/dev/sndstat，可以发现，最初默认设置0，使用的pcm0，也就是后面板的声音插孔，可是我的耳机插的是前面板，对应的是1，一般小耳机都插前面板，线也没那么长，不过插前面板有可能受USB设备的影响而又杂音。



