---
uuid:             3fcd5c28-1afd-420c-97bf-79f05b950b02
layout:           post
title:            Shell数值计算
slug:             shell-calculation
subtitle:         null
date:             '2014-11-12T22:37:54.000Z'
updated:          '2015-07-29T22:42:54.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---



## 一，整数计算

### 1, 使用let

```
# 注意，后半段不能有空格，变量引用不用加$
let x=3+4
let x=x*2
```

###  2, 使用$((…))

```
# 这里使用空格也是没有问题的，变量引用不用加$
x=$((3+2))
x=$((x+2))
```

###  3, 使用expr

```
# 需要使用空格分割开操作数和操作算子，部分算子由于跟shell的符号冲突，需要使用转义，
# 调用外部程序计算，不像1,2使用shell的内部机制，所以变量的引用需要使用$，
x=`expr 3 + 4`
x=`expr $x * 4`
```


##  二，浮点计算

### 1, 使用bc

```
# 需要设置scale精度，否则结果还是整数，空格加不加无所谓
# 所以用bc也可以做整数计算，不过却是大材小用
x=`echo "scale=3;4/3" | bc`
x=`echo "scale=3;$x/3" | bc`
```

 



