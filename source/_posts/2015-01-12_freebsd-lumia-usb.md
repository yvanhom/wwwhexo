---
uuid:             0727fc8b-91f9-4db3-8cee-8aca25923d1d
layout:           post
title:            '在FreeBSD上读取Lumia设备 -- 失败'
slug:             freebsd-lumia-usb
subtitle:         null
date:             '2015-01-12T03:07:00.000Z'
updated:          '2015-07-29T22:39:55.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---


用的是Lumia手机，本来可以走onedrive网络流程来实现手机跟PC共享文件，可是国内onedrive太慢了，还不得不翻墙。

其它方法由于Lumia的应用缺乏，WP系统的封闭性，公开的API，权限少。

总之还是使用USB连接比较快速。

由于PC使用的是FreeBSD，插上Lumia手机后没有像插入U盘那样，观察到/dev/da0，以为不能用。

后来google “lumia Linux”，结果发现可以使用mtpfs。

pkg search mtpfs 也找到可用的package，赶紧尝试下。

找到的package有fusefs-simple-mtpfs-0.2.s20140709*2，mtpfs-1.1*2，前者标注是2014年的，后者在相关网站上找，发现已经几年没更新了，抱着买新不买旧的想法，先试试simple-mtpfs。

1. 安装fusefs-simple-mtpfs：pkg install fusefs-simple-mtpfs
2. 加载fuse模块：kldload fuse
3. 插入设备，看看有没有识别到：simple-mtpfs -l，显示没有设备，mtp-detect，还是没有设备，mtp-connect依然没有设备，到此尝试失败。中间还报过错误，显示没有.mtpz-data文件，从github simple-mtpfs中下载了一个，放在/root目录下，错误没有了，设备依然没有显示。

此次尝试以失败告终，失败原因不明，不知道该怎么排查，于是就这样搁置吧，反正也不是那么必要。



