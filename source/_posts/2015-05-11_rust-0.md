---
uuid:             25b51658-5675-4927-809e-58ae8acedc61
layout:           post
title:            '学习 Rust 语言 -- 搭建环境'
slug:             rust-0
subtitle:         null
date:             '2015-05-11T04:45:06.000Z'
updated:          '2015-07-29T22:34:32.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - rust

---


### FreeBSD

在 FreeBSD 上搭建 Rust 开发环境。

第一步，安装 Rust 编译器，搜索 Rust 看看有没有可以直接使用的 package：

`pkg search rust`

发现只有 0.12 版本，[官网](www.rust-lang.org)上最新是 1.0.0-beta.4，所以还是直接编译源码吧。

常规流程：

```
wget https://static.rust-lang.org/dist/rustc-1.0.0-beta.4-src.tar.gz
tar xvzf rustc-1.0.0-beta.4-src.tar.gz
cd rust-1.0.0-beta.4
./configure
make
```

都很顺利，不过 make 失败，稍微查查便会发现 rustc 源码包使用 GNU Makefile 系统，跟 FreeBSD 的 Makefile 不一致，所以改成

`gmake`

gmake 过程非常久，看网上所说，Rust 的编译速度比 C++ 还可怕，所以耐心地等吧。编译完成后，安装

`gmake install`

写个 Hello, World! 看看：

```
fn main() {
    println!("Hello, world!");
}
```

编译运行：

```
rustc main.rs
./main
```

一切顺利。 接下来安装 Cargo，结果发现 Cargo 没有可用的 package，官方源码需要使用 Cargo 编译，官方提供的二进制打包没有 FreeBSD 平台的。

google 搜索解决方案，发现了[这个](http://csperkins.org/research/misc/2015-01-02-cargo-freebsd.html)，可是同样的平台，同样的 FreeBSD 版本却运行失败。细看了一下代码，发现是在迭代编译，最初的 Cargo 肯定不是使用 Cargo 编译的，所以先编译最初的 Cargo，然后一个一个 Patch 打过来，用编译的老 Cargo 编译新的Cargo，不断迭代，直到最新版本的 Cargo。 迭代编译时间需要很久，因为还需要编译不同版本的 Rust，等待了 N 久，报错，不是我能快速解决的问题，真是沮丧。

转战远程平台。

### Koding

Koding 是我最常用的在线编程平台，虽然如果没在网页上操作的话，Koding 会自动将虚拟机关掉，SSH 的操作不算，所以使用比较麻烦。

不过我还是很喜欢 Koding，因为给的权限很大，可以做任何事情（除了阻止自动关机）。

Koding 提供的虚拟机安装的是 *Ubuntu 14.04 LTS*，在 Ubuntu 上搭建 Rust 环境可以使用一键脚本，快捷方便：

`curl -sf -L https://static.rust-lang.org/rustup.sh | sh`

很顺利，也很快，因为没有编译过程。真羡慕使用 Ubuntu 平台的人。好歹 Mozilla 的几个软件都是跨平台的，不知什么时候 Rust 的 Cargo 也能在 FreeBSD 上快捷安装使用。

尝试一下 Cargo 的使用。 首先新建项目：

`cargo new CargoHello --bin`

生成最初的目录文件树如下：

```
./CargoHello/
./CargoHello/src
./CargoHello/src/main.rs
./CargoHello/.gitignore
./CargoHello/Cargo.toml
./CargoHello/.git
./CargoHello/.git/description
./CargoHello/.git/info
./CargoHello/.git/info/exclude
./CargoHello/.git/HEAD
./CargoHello/.git/refs
./CargoHello/.git/refs/tags
./CargoHello/.git/refs/heads
./CargoHello/.git/hooks
./CargoHello/.git/hooks/update.sample
./CargoHello/.git/hooks/commit-msg.sample
./CargoHello/.git/hooks/prepare-commit-msg.sample
./CargoHello/.git/hooks/pre-rebase.sample
./CargoHello/.git/hooks/post-update.sample
./CargoHello/.git/hooks/applypatch-msg.sample
./CargoHello/.git/hooks/pre-applypatch.sample
./CargoHello/.git/hooks/pre-commit.sample
./CargoHello/.git/hooks/pre-push.sample
./CargoHello/.git/objects
./CargoHello/.git/objects/info
./CargoHello/.git/objects/pack
./CargoHello/.git/config
```

文件数不少，连 git 都包含进去了，赞！ 生成的代码便是 HelloWorld，不需要修改，直接编译运行：

```
cargo build
cargo run
```

简单截个运行结果：

```
yvanhom: ~/Rust/CargoHello $ cargo build
   Compiling CargoHello v0.1.0 (file:///home/yvanhom/Rust/CargoHello)
yvanhom: ~/Rust/CargoHello $ cargo run
     Running `target/debug/CargoHello`
Hello, world!
```

到此 Rust 开发环境搭建完成，接下来便好好学习这一门新语言。 相信 Rust，因为相信 Mozilla，相信 Firefox，相信开源。



