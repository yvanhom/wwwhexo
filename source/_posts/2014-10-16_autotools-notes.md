---
uuid:             7009fad5-43a3-4ae5-8d6b-8296478730ed
layout:           post
title:            AutoTools流程速记
slug:             autotools-notes
subtitle:         null
date:             '2014-10-16T23:14:39.000Z'
updated:          '2015-07-29T22:43:37.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---



## 1, 运行autoscan，生成configure.scan


## 2, 将configure.scan重命名为configure.ac


## 3, 修改configure.ac

原内容如下：

```
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.
 
AC_PREREQ([2.69])
AC_INIT([FULL-PACKAGE-NAME], [VERSION], [BUG-REPORT-ADDRESS])
AC_CONFIG_SRCDIR([main.c])
AC_CONFIG_HEADERS([config.h])
 
# Checks for programs.
AC_PROG_CC
 
# Checks for libraries.
 
# Checks for header files.
AC_CHECK_HEADERS([stdlib.h unistd.h])
 
# Checks for typedefs, structures, and compiler characteristics.
 
# Checks for library functions.
AC_FUNC_MALLOC
 
AC_OUTPUT
```

修改后如下：

```
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.
 
AC_PREREQ([2.69])
AC_INIT([xyw-pi], [1.0], [ywxie@hust.edu.cn])
AC_CONFIG_SRCDIR([main.c])
AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE
 
 
# Checks for programs.
AC_PROG_CC
 
# Checks for libraries.
AC_CHECK_LIB(pthread, [pthread_create])
 
# Checks for header files.
AC_CHECK_HEADERS([stdlib.h unistd.h])
 
# Checks for typedefs, structures, and compiler characteristics.
 
# Checks for library functions.
AC_FUNC_MALLOC
 
AC_OUTPUT([Makefile])
```

主要修改包括：

- 第5行，设置包名，版本，邮箱
- 第8行，增加AM_INIT_AUTOMAKE
- 第15行，增加pthread这个lib的依赖
- 第25行，输出Makefile


## 4, 运行aclocal生成aclocal.m4


## 5, 运行autoheader生成config.h.in


## 6, 编写Makefile.am

内容如下：

```
bin_PROGRAMS=xywpi
xywpi_SOURCES=main.c
```

主要是设置生成二进制文件xywpi，该文件由源文件main.c编译生成


## 7, 编写AUTHORS, NEWS, README, ChangeLog等

    touch NEWS AUTHORS ChangeLog README

这里只生成空文件


## 8, 运行automake –add-missing生成一堆需要的文件


## 9, 运行autoconf生成configure


编译过程很熟悉了，这里也写一下：

## 10, configure生成Makefile

- configre –help查看选项


## 11, make编译

- make clean清楚生成数据
- make distclean清楚生成数据+Makefile
- make install安装
- make uninstall卸载
- make dist打包成tar.gz



