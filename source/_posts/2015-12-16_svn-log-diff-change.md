---
uuid:             b250d9f1-9d50-4a5a-90a9-9d51c9133b09
layout:           post
title:            'Subversion 中获取详细修改变更'
slug:             svn-log-diff-change
subtitle:         null
date:             '2015-12-16T22:04:38.000Z'
updated:          '2015-12-16T22:05:11.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---

最近被 boss 派发任务，去针对别人写的代码整理出一份说明文档。

代码使用 svn 管理，他的代码主要在一个分支内，整理需要查看他究竟改了哪些部分。

唉，现在 git 大行其道，svn 我都忘记怎么使用了，而且还没有 GUI 使用（其实是我真的很少使用 svn，所以懒得在工作机子上安装 svn 的 GUI 客户端），直接使用服务器上的 svn。

一番搜索，才搞定，这里记录一下。

首先是获取有哪些提交，git 中是 commit，而 svn 中用 Revision，命令：

```sh
svn log
```

某个 revision 更删改了哪些路径：

```sh
svn log -v -r 4426
```

某个 revision 的 更改，即 diff：

```sh
svn diff -c 4426
```

这三条命令，基本上就足够我完成工作了。
