---
uuid:             88f029fe-592d-46be-968d-1a699b97b4b6
layout:           post
title:            使用gettext实现i18n
slug:             gettext-localization
subtitle:         null
date:             '2015-01-06T04:27:51.000Z'
updated:          '2015-07-29T22:40:36.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - freebsd

---


i18n是internationalization，翻译为国际化，这个缩写真有趣，取头尾字母，18表示中间共有18个字母。

类似的含义缩写有l10n（localization，本地化），g11n（globalization，全球化）等。

这些都是为了方便软件的翻译工作，最好是提供一种机制，使得支持某种本地化语言不需要修改程序。

这里介绍使用GNU Gettext工具来提供软件的i18n支持。

### 1, 安装gettext

在FreeBSD下运行：

    pkg install gettext

### 2, 编写示例程序

main.c源代码如下：

````
#include<stdio.h>
#include<locale.h>
#include<libintl.h>
 
/*简化书写*/
#define _(STRING) gettext(STRING)
 
const char *DOMAIN = "helloworld";       /*最后的mo文件名*/
const char *DIRNAME = "lang";            /*文件所在的目录*/
 
int main(int argc, char **argv) {
        setlocale(LC_ALL, "");           /*使用系统默认环境变量*/
        bindtextdomain(DOMAIN, DIRNAME); /*绑定文本域*/
        textdomain(DOMAIN);              /*使用文本域*/
 
        printf(_("Hello, %s!n"), _("World"));
        return 0;
}
````

编译：

    cc -I/usr/local/include/ -L/usr/local/lib/ -lintl main.c -o helloworld

### 3, 生成pot文件

使用xgettext生成helloworld.po文件

    xgettext -k_ --package-name helloworld --package-version 0.1 ../*.c -o helloworld.pot

其中

- -k_ 指定寻找被_(“”)所包含的文本，不指定时寻找gettext(“”)
- ../*.c 指定所有的源文件

4, 为需要的语言生成po文件

这里使用msginit生成了两种语言的po文件，都使用UTF-8编码。

```
msginit -l ja_JP.UTF-8 -i helloworld.pot # 生成ja.po 
msginit -l zh_CN.UTF-8 -i helloworld.pot # 生成zh_CN.po
```

zh_CN.po如下：

```
# Chinese translations for helloworld package
# helloworld 软件包的简体中文翻译.
# Copyright (C) 2015 THE helloworld'S COPYRIGHT HOLDER
# This file is distributed under the same license as the helloworld package.
# Xie YW <padicao@localhost>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: helloworld 0.1n"
"Report-Msgid-Bugs-To: n"
"POT-Creation-Date: 2015-01-06 15:39+0800n"
"PO-Revision-Date: 2015-01-06 17:07+0800n"
"Last-Translator: Xie YW <padicao@localhost>n"
"Language-Team: Chinese (simplified)n"
"Language: zh_CNn"
"MIME-Version: 1.0n"
"Content-Type: text/plain; charset=UTF-8n"
"Content-Transfer-Encoding: 8bitn"
 
#: ../main.c:13
#, c-format
msgid "Hello, %s!n"
msgstr ""
 
#: ../main.c:13
msgid "World"
msgstr ""
```

ja_JP.po如下：

```
# Japanese translations for helloworld package
# helloworld パッケージに対する英訳.
# Copyright (C) 2015 THE helloworld'S COPYRIGHT HOLDER
# This file is distributed under the same license as the helloworld package.
# Xie YW <padicao@localhost>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: helloworld 0.1n"
"Report-Msgid-Bugs-To: n"
"POT-Creation-Date: 2015-01-06 15:39+0800n"
"PO-Revision-Date: 2015-01-06 16:38+0800n"
"Last-Translator: Xie YW <padicao@localhost>n"
"Language-Team: Japanesen"
"Language: jan"
"MIME-Version: 1.0n"
"Content-Type: text/plain; charset=UTF-8n"
"Content-Transfer-Encoding: 8bitn"
"Plural-Forms: nplurals=1; plural=0;n"
 
#: ../main.c:13
#, c-format
msgid "Hello, %s!n"
msgstr ""
 
#: ../main.c:13
msgid "World"
msgstr ""
```

###  4, 翻译po文件

在指定域内进行翻译。

比如zh_CN.po翻译为：

```
msgid "Hello, %s!n"
msgstr "你好，%s!n"
 
msgid "World"
msgstr "世界"
```

ja.po翻译为：

```
msgid "Hello, %s!n"
msgstr "こんにちは、%s！n"
 
msgid "World"
msgstr "世界"
```

### 5, 生成mo文件

使用msgfmt生成指定mo文件：

```
msgfmt zh_CN.po -o zh_CN.mo
msgfmt ja.po -o ja.mo
```

###  6, 将文件放置到合适位置

文件的放置位置已经在bindtextdomain中用DIRNAME指明。

```
├── helloworld
├── lang
│   ├── ja
│   │   └── LC_MESSAGES
│   │       └── helloworld.mo             （原ja.mo）
│   └── zh_CN
│       └── LC_MESSAGES
│           └── helloworld.mo              (原zh_CN.mo)
```

### 7, 运行

```
LC_ALL=zh_CN.UTF-8 ./helloworld
# 输出      你好，世界!
./helloword
# 默认环境便是zh_CN.UTF-8，所以输出同上
LC_ALL=ja_JP.UTF-8 ./helloworld
# 输出      こんにちは、世界！
LC_ALL=C ./helloworld
# 其它地域设置代表输出英文
# 输出      Hello, World!
```

注意：

- 看网上的文档，设置LC_MESSAGES就可以使用不同的本地化设置，可是在我的测试中，只有设置LC_ALL才起效
- 由于使用UTF-8编码，可是zh_CN和jp的默认编码都不是UTF-8，所以，运行时需要指定完整的本地化设置，如果只是LC_ALL=zh_CN/jp/jp_JP，都会显示乱码。



