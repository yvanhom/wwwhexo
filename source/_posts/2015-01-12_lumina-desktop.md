---
uuid:             491b0056-3189-4658-bd24-723e8e70e2e2
layout:           post
title:            Lumina初步使用感想
slug:             lumina-desktop
subtitle:         null
date:             '2015-01-12T03:00:23.000Z'
updated:          '2015-07-29T22:40:05.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd

---


突然看到BSD出了一款桌面，轻量级，于是就忍不住在FreeBSD下尝试。

为什么会看到这条新闻？说来巧合，当时我搜索的是“FreeBSD Lumia”，希望在FreeBSD上能够识别Lumia的手机存储，结果意外的看到Lumina，不禁起了兴趣。

[Lumina的新闻](http://code.csdn.net/news/2819525 "Lumina新闻")吸引我的地方有几个关键字：BSD，Qt，轻量级。

BSD协议我一直很喜欢；Qt开发我也学习过，很实用很强大，一提到Qt的桌面，就想到重量级的KDE，KDE我也喜欢用，不过太重量级了，占用内存大，经常崩溃，时不时弹出要不要发送BUG报告。

当然轻量级的Qt桌面，除了Lumina，还有LxQt正在开发，期待中。

1, 第一感觉确实是非常的轻量级，安装的东西很少，当然，与之相对的便是非常的不完善，缺少很多东西，发展的道路还很长。

默认的桌面主题，窗口主题真心不敢恭维，或者说是不合我的审美，于是换掉，将主题换成空，在QT中设置主题，不过Qt提供的主题都也不合我的口味，于是还是使用Lumina的默认主题。窗口主题，候选选择太多了，于是便懒得选了，继续默认。于是全都默认，= =!!!。

然后一些配置找不到地方，想修改窗口Title的字体，结果找不到，窗口主题文件太长，用vim打开的话没准我还会找找。

2, 默认使用xterm作为终端，于是设置了字体，学习了复制粘贴方法，不过，熟悉了gnome，xfce，kde下的华丽终端的我，还是很不习惯，要不要换呢？

3, 开始菜单，或者叫做用户菜单，可以快速收藏应用，文件夹，文件，并直接查看，感觉确实不错；可以直接在菜单上浏览目录内容，也很赞。

3, 还没有国际化翻译，在/usr/local/share/Lumina-DE/i18n中，可以看到，只有英文版本，其它语言文件全是空白，自己当时刚好有空，于是就利用Qt 4 Linguist粗糙地翻译了一下，只有几百个词句，很快便完成，于是发布，并替换掉原来的空白qm文件，便有中文版本可以用了，翻译后的结果可以到github看看[这个](https://github.com/padicao2010/lumina-i18n "github lumina-i18n")。

不过，Lumina-DE没有翻译成功，看DEBUG log，显示使用zh_CN，可是并没有加载翻译后的qm文件，还是显示英文，算是BUG吧。

4, Lumina的文件管理器lumina-fm真心粗糙，让我忍不住又想吐槽Lumina的默认主题，不过居然是很少见的多标签页浏览，赞，也很激进地，不支持多窗口。

5, 桌面插件不多，不过只有Note深得我意，其它就不用了。

6, 面板（屏幕边缘的栏目）插件也不多，基本都显示，还算直观。

总之，PC-BSD团队加油，让Lumina更加完善，希望跟即将到来的LxQt各有特色，平分秋色。



