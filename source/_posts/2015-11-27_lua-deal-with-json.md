---
uuid:             2285038d-2144-480e-88a8-3dcb50aa551f
layout:           post
title:            'Lua 的 JSON 处理'
slug:             lua-deal-with-json
subtitle:         null
date:             '2015-11-27T07:11:16.000Z'
updated:          '2015-11-27T07:13:18.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---

快速处理分析文本数据，我一般用 shell，使用 grep，awk等，如果 shell 不能用或不好用不易用， 便使用 lua。最近需要对通过 RestAPI 获得的数据进行处理，数据格式是 json，shell 貌似没有直接解析 json 格式的命令，所以这里使用 lua 来进行处理。

Lua 内建的也没有 json 库，不过可以从网上找，我找到的是 [json4lua](https://github.com/craigmj/json4lua)，纯 lua 实现，使用方便。

首先将 [json.lua](https://github.com/craigmj/json4lua/blob/master/json/json.lua) 文件下载下来，然后 

```lua
local json = require("json")
```

json4lua 暴露的接口很少，只有两个

```
-- 从 lua table 编码成 lua string
local str = json.encode({ x = 10, y = 20 })

-- 从 lua string 编码成 lua table
local t = json.decode(str)
```

lua table 跟 json 字符串的对应很自然，无需多言，数组用数字索引的 table，键值对用文本索引的 table。

回到我的例子，向 floodlight 调用 RestAPI，获取交换机之间的链接，以下是需要处理的 json 的数据：

```json
[
    {
        "dst-port": 1,
        "dst-port-state": 0,
        "dst-switch": "00:00:6a:e4:22:b3:14:4f",
        "src-port": 4,
        "src-port-state": 0,
        "src-switch": "00:00:2e:44:38:dd:59:40",
        "type": "internal"
    },
    {
        "dst-port": 5,
        "dst-port-state": 0,
        "dst-switch": "00:00:2e:44:38:dd:59:40",
        "src-port": 5,
        "src-port-state": 0,
        "src-switch": "00:00:7a:ed:e0:92:30:4a",
        "type": "internal"
    }
    // 还有很多，这里省略
]
```

以下是处理的 lua 脚本：

```lua
local json = require("json")

-- 读取文件并解析
local filename = arg[1]
local file = io.open(filename)
local str = file:read("*all")
local links = json.decode(str)

local switches = {}

local function updateSrcSwitch(src, srcport, dest)
        local sw = switches[src]
        if not sw then
                sw = {}
                switches[src] = sw
        end
        sw[srcport] = dest
end

-- 遍历所有连接
for i, li in ipairs(links) do
        local src = li["src-switch"]
        local dst = li["dst-switch"]
        local srcport = li["src-port"]
        local dstport = li["dst-port"]

        updateSrcSwitch(src, srcport, dst)
        updateSrcSwitch(dst, dstport, src)
end

local function countPort(sw)
        local result = 0
        for k, v in pairs(sw) do
                result = result + 1
        end
        return result
end

-- 输出只有一个端口的交换机及其端口号
print("return {")
for k, v in pairs(switches) do
        if countPort(v) == 1 then
                for port, dest in pairs(v) do
                        print(string.format("    { '%s', %d },", k, port))
                end
        end
end
print("}")
```

以下是输出结果：
```lua
return {
    { '00:00:6a:e4:22:b3:14:4f', 1 },
    { '00:00:8a:b0:f0:02:a5:42', 1 },
    { '00:00:9a:b4:46:a5:a1:43', 1 },
    { '00:00:66:a7:71:2e:59:41', 1 },
    { '00:00:ba:fd:9e:7e:e9:41', 1 },
    { '00:00:52:9c:26:5a:76:47', 1 },
    { '00:00:d6:20:f5:e1:9e:4b', 1 },
    { '00:00:92:16:f2:ea:fc:40', 1 },
}
```

