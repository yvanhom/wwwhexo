---
uuid:             ebb90b05-0c87-44d5-9804-8e2251660503
layout:           post
title:            HDFS写流程源码分析（一）
slug:             hdfs-write
subtitle:         null
date:             '2014-07-25T01:02:59.000Z'
updated:          '2016-07-06T03:13:02.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - java

---



## 1, 说明

在Hadoop2.3.0下进行源码分析。整理了一下，流程简单的说就是下边的图中所示：

![HDFS 写流程概况](http://o9sak9o6o.bkt.clouddn.com/techs/hdfs-read-write-1.jpg)


## 2, 几个参数

<span style="color: #ff6600;">**dfs.bytes-per-checksum**: 默认512字节，每达到chunk大小，计算一次校验码</span>

<span style="color: #ff6600;">**dfs.client-write-packet-size**: 默认64KB，每达到packet大小，发送一次数据包，包含原始数据和校验码．（做了些修改，后边可能没跟上，以后修正）</span>

**dfs.blocksize**: 默认128M，HDFS的块大小，每次发送的数据大小达到blocksize时，会发送一个空packet来结束该block的创建

**dfs.stream-buf-size**: 默认4K，HDFS写数据时流式BUFFER的大小，必须比dfs.bytes-per-checksum大


## 3, 写数据块的DFSClient端流程

流程图如下所示：

![ HDFS写流程下Client端流程](http://o9sak9o6o.bkt.clouddn.com/techs/hdfs-read-write-2.jpg)

当写入的数据达到dfs.bytes-per-checksum时，会分配一个packet，并将之加入发送队列（如果队列已满，会等待），代码如下：

```
// DFSOutputStream.java:1533
void writeChunk(byte[] b, int offset, int len, byte[] checksum) {
  currentPacket = new Packet(...);             // 分配新packet
  currentPacket.writeChecksum(checksum, 0, cklen);
  currentPacket.writeData(b, offset, len);
  bytesCurBlock += len;
  waitAndQueueCurrentPacket();                 // 加入处理队列
  if (bytesCurBlock == blockSize) {            // 达到块大小阈值，发送空packet
    currentPacket = new Packet(0, 0, bytesCurBlock);
    currentPacket.lastPacketInBlock = true;
    currentPacket.syncBlock = shouldSyncBlock;
    waitAndQueueCurrentPacket();
    bytesCurBlock = 0;
    lastFlushOffset = 0;
  }
}
```

加入队列的Packet由同文件下的DataStreamer进行处理（4～9），创建PipeLine，发起对第一个DataNode的连接，同时启动ResponseProcessor处理ACK回复（11～18）。如果这是最后一个包，那么将阻塞等待先前发送的所有数据包的ACK确认完成 （20～28）。紧接着发送数据包（30～37）。发送完成后，如果这是最后一个包，那么将阻塞等待该包的确认信息到来，然后重置PipeLine，关闭连接，关闭ResponseProcessor，准备处理下一个数据块（39～49）。

```
// DFSOutputStream.DataStreamer : 463
public void run() {
  while(...) {
    // 从队列中获取要处理的Packet
    if (dataQueue.isEmpty()) {
      one = new Packet();  // 发送心跳包
    } else {
      one = dataQueue.getFirst(); // regular data packet
    }
 
    // 初始，块的创建/数据添加，创建PipeLine
    if (stage == BlockConstructionStage.PIPELINE_SETUP_CREATE) {
      setPipeline(nextBlockOutputStream());
      initDataStreaming();
    } else if (stage == BlockConstructionStage.PIPELINE_SETUP_APPEND) {
      setupPipelineForAppendOrRecovery();
      initDataStreaming();
    }
 
    // 如果这是最后一个包（close包），那么在发送前等待先前所有Packet的确认完成
    if (one.lastPacketInBlock) {
      synchronized (dataQueue) {
        while (ackQueue.size() != 0 && ...) {
          dataQueue.wait(1000);
        }
      }
      stage = BlockConstructionStage.PIPELINE_CLOSE;
    }
 
    // 心跳包的处理有所不同，这里略过
    dataQueue.removeFirst();
    ackQueue.addLast(one);
    dataQueue.notifyAll();
 
    // 发送数据
    one.writeTo(blockStream);
    blockStream.flush();
 
    // 如果这是最后一个包，等待该包的确认完成
    if (one.lastPacketInBlock) {
      synchronized (dataQueue) {
        while (ackQueue.size() != 0) {
          dataQueue.wait(1000);// wait for acks to arrive from datanodes
        }
      }
 
      // 重置PipeLine，关闭连接
      endBlock();
    } 
  }
}
```

更详细地，创建PipeLine的流程：如果是新数据块，那么就需要调用NameNode的RPC为该块分配新位置（2～13），否则只需要调用NameNode的RPC获取该块的已分配位置（15～21）；有了副本位置后，便可以调用createBlockOutputStream来与第一个DataNode进行连接，发送写数据块的请求，并获取请求确认ACK（23～41）。

```
// DFSOutputStream.DataStreamer
// 为新的数据块创建PipeLine
private LocatedBlock nextBlockOutputStream() {
  lb = locateFollowingBlock(...);   // 见下，为该数据块分配位置
  nodes = lb.getLocations();        // 获取副本的分配位置
  success = createBlockOutputStream(nodes, 0L, false);  // 发起对第一个DataNode的连接
  return lb;
}
 
private LocatedBlock locateFollowingBlock(...) {
  // 调用NameNode的RPC为之分配一个新的数据块位置
  return dfsClient.namenode.addBlock(...); 
}
 
// 为现有数据块添加数据，创建PipeLine
private boolean setupPipelineForAppendOrRecovery() {
  // 调用NameNode的RPC获取数据块的副本位置
  LocatedBlock lb = dfsClient.namenode.updateBlockForPipeline(...);
  nodes = lb.getLocations();        // 获取副本的分配位置
  success = createBlockOutputStream(nodes, newGS, isRecovery);
}
 
// 连接第一个DataNode
private boolean createBlockOutputStream(...) {
  s = createSocketForPipeline(nodes[0], nodes.length, dfsClient); // 发起TCP Socket连接，并设置
  OutputStream unbufOut = NetUtils.getOutputStream(s, writeTimeout);
  InputStream unbufIn = NetUtils.getInputStream(s);
  out = new DataOutputStream(new BufferedOutputStream(unbufOut,
              HdfsConstants.SMALL_BUFFER_SIZE));
  blockReplyStream = new DataInputStream(unbufIn);
 
  // 发送写数据块请求，这里只发送后两个节点的位置信息过去，具体看源代码
  new Sender(out).writeBlock(block, accessToken, dfsClient.clientName,
              nodes, null, recoveryFlag? stage.getRecoveryStage() : stage, 
              nodes.length, block.getNumBytes(), bytesSent, newGS, checksum,
              cachingStrategy.get());
 
  // 接收该请求的确认ACK
  BlockOpResponseProto resp = BlockOpResponseProto.parseFrom(
              PBHelper.vintPrefixed(blockReplyStream));
}
```

ResponseProcessor负责接收确认ACK，接收到来自第一个节点的ACK包后，会遍历判断是否所有副本节点都正常地写入数据，如果异常，将抛出IOException。正常情况下，紧接着如果是心跳包的回复，那么ResponseProcessor将忽略掉，否则处理数据包的确认。

```
// DFSOutputStream.DataStreamer.ResponseProcessor : 716
public void run() {
  PipelineAck ack = new PipelineAck();
  while(...) {
    ack.readFields(blockReplyStream);
 
    long seqno = ack.getSeqno();
    // 对所有副本节点进行检查
    for (int i = ack.getNumOfReplies()-1; i >=0  && dfsClient.clientRunning; i--) {
      final Status reply = ack.getReply(i);
      if (reply != SUCCESS) {
        throw new IOException(...);
      }
    }
 
    if (seqno == Packet.HEART_BEAT_SEQNO) {  // 忽略心跳包的回复ACK
      continue;
    }
    Packet one = ackQueue.getFirst();
    block.setNumBytes(one.getLastByteOffsetBlock()); // 更新已经确认的数据
    ackQueue.removeFirst();   // 完成该次数据Chunk的确认
  }
}
```

当处理完一个数据块后，DataStreamer会调用endBlock重置PipeLine，准备下一个数据块的处理。

```
// 数据块写入完成的处理，准备处理下一个数据块
private void endBlock() {
  closeResponder();         // 关闭ResponseProcessor
  closeStream();            // 关闭跟第一个DataNode的连接
  setPipeline(null, null);  // 重置PipeLine
  stage = BlockConstructionStage.PIPELINE_SETUP_CREATE;  // 回到最初状态，准备创建新的数据块
}
```

 



