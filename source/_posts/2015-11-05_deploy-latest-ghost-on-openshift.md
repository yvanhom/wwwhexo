---
uuid:             be930550-eb39-433b-ba44-01a045dd90b3
layout:           post
title:            '在 OpenShift 上部署 Ghost 最新版本'
slug:             deploy-latest-ghost-on-openshift
subtitle:         null
date:             '2015-11-05T07:57:36.000Z'
updated:          '2015-11-05T07:57:36.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

OpenShift 上的 Ghost 版本不会自动更新，自己动手更新，丰衣足食。

参考 [HOW TO UPGRADE GHOST ON OPENSHIFT](http://www.cnblogs.com/chendeshen/p/4189953.html)，不过增加了一些步骤。

**Step 1.** 在 OpenShift 上新建 Node.js 0.10 应用

**Step 2.** 通过 git clone 到本地

``` sh
git clone ssh://*****@***.rhcloud.com/~/git/***.git/
```

**Step3.** 将刚 clone 的所有文件，除了 .git 和 .openshift 外，全部删除

**Step4.** 下载最新版本 Ghost Blog，当前是 0.7.1，并解压到 clone 的目录里。

**Step5.** 修改 config.js，内容见[这里](https://github.com/openshift-quickstart/openshift-ghost-quickstart/blob/master/config.js)

**Step6.** 进一步修改 config.js，指定 url：

```javascript
url: 'http://www.example.com',
```

**Step7.** 修改 package.json 中的 main，从 `./core/index` 换成 `index`：

```json
"main": "index",
```

**Step8.** 提交

```sh
git add .
git commit -m "Init Ghost"
git push
```

**Step8.** 提交会提示启动失败，原因未明，此时随意修改，比如加个注释，再提交，第二次提交便成功了。
