---
uuid:             cfa67ee9-a1b7-4012-bc63-508dfd1253d5
layout:           post
title:            为xdg-open设置默认应用程序
slug:             x-window-default-open
subtitle:         null
date:             '2014-09-28T23:46:50.000Z'
updated:          '2015-07-29T22:44:20.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - freebsd

---



## 一，说明

在Linux X-Window下，所有程序安装基本都会生成一个desktop的快捷方式的文件，保存在/usr/share/applications目录下，然后当前每个文件格式的默认关联程序保存在~/.local/share/applications/defaults.list中，这里保存每个用户的自定义修改，格式如下：

    application/pdf=okular.desktop

所以，用户可以在这里添加默认程序的关联来覆盖系统设置，不过也可以使用xdg-mime来设置，最终影响这个文件。


## 二，使用xdg-mime来设置

首先获取指定格式的默认程序关联，可以看到系统默认使用gimp来打开pdf文件，这是很糟糕的：

```
xdg-mime query filetype myfile.pdf
# Output application/pdf
 
xdg-mime query default application/pdf
# Output: gimp.desktop gimp.desktop
```

其次设置指定格式的默认应用程序关联：

```
# 指定程序的desktop文件的名称一般是 名称.desktop，如果不确定可以到/usr/share/applications里头找
xdg-mime default okular.desktop application/pdf
```

这样子，用xdg-open打开pdf文件就相当于用okular来打开。



