---
uuid:             c07b1e2d-072e-4a72-a29e-adbadd2c905c
layout:           post
title:            非root用户使用virt-manager
slug:             nonroot-virtmanager
subtitle:         null
date:             '2015-04-12T21:43:08.000Z'
updated:          '2015-07-29T22:36:32.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


想要用非root用户使用virt-manager，那么需要在policy上增加设置，做到提权。

方法是新建文件/etc/polkit-1/rules.d/50-org.libvirt.unix.manage.rules，内容如下：

```
polkit.addRule(function(action, subject) {
  if (action.id == "org.libvirt.unix.manage" && subject.isInGroup("wheel")) {
    return polkit.Result.YES; 
  } 
});
```

于是所有在wheel组内的用户都能使用virt-manager。



