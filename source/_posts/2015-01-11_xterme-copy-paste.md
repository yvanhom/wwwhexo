---
uuid:             f4e57358-b34a-4a73-88f1-0869a00ef007
layout:           post
title:            xterm字体与复制粘贴
slug:             xterme-copy-paste
subtitle:         null
date:             '2015-01-11T22:57:02.000Z'
updated:          '2015-07-29T22:40:23.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - freebsd

---


尝试在FreeBSD使用PC-BSD开发的Lumina，也就顺便使用xterm作为默认的终端。

不过默认的xterm蛮丑的，中文字体也默认不能显示，需要 Ctrl + 右键 调出设置菜单，选择Large字体，才行。

于是百度，发现关于这个的都年代久远，虽然都能用，于是使用[这个](http://foyon.blog.163.com/blog/static/2010342752011112910554591/ "xterm字体设置")的设置。

### 1, 在~/.Xresources中设置xterm字体

新建~/.Xresources文件，内容如下：

```
! % xrdb -load ~/.Xresources
! % xrdb -query
! % xrdb -symbol
! % xrdb -merge ~/.Xresources
 
! XTerm config
XTerm*locale: true
 
XTerm*fontMenu*fontdefault*Label: Default
XTerm*font: -misc-fixed-medium-r-normal-*-18-120-100-100-c-90-iso10646-1
XTerm*wideFont: -misc-fixed-medium-r-normal-*-18-120-100-100-c-180-iso10646-1
 
XTerm*font1.Label: efont 12 pixel
XTerm*font1: -efont-fixed-medium-r-normal-*-12-120-75-75-c-60-iso10646-1
XTerm*wideFont1: -efont-fixed-medium-r-normal-*-12-120-75-75-c-120-iso10646-1
XTerm*font2.Label: misc 13 pixel
XTerm*font2: -misc-fixed-medium-r-semicondensed-*-13-120-75-75-c-60-iso10646-1
XTerm*wideFont2: -misc-fixed-medium-r-normal-*-13-120-75-75-c-120-iso10646-1
XTerm*font3.Label: efont 14 pixel
XTerm*font3: -efont-fixed-medium-r-normal-*-14-140-75-75-c-70-iso10646-1
XTerm*wideFont3: -efont-fixed-medium-r-normal-*-14-140-75-75-c-140-iso10646-1
XTerm*font4.Label: efont 16 pixel
XTerm*font4: -efont-fixed-medium-r-normal-*-16-160-75-75-c-80-iso10646-1
XTerm*wideFont4: -efont-fixed-medium-r-normal-*-16-160-75-75-c-160-iso10646-1
XTerm*font5.Label: misc 18 pixel
XTerm*font5: -misc-fixed-medium-r-normal-*-18-120-100-100-c-90-iso10646-1
XTerm*wideFont5: -misc-fixed-medium-r-normal-*-18-120-100-100-c-180-iso10646-1
XTerm*font6.Label: efont 24 pixel
XTerm*font6: -efont-fixed-medium-r-normal-*-24-240-75-75-c-120-iso10646-1
XTerm*wideFont6: -efont-fixed-medium-r-normal-*-24-240-75-75-c-240-iso10646-1
 
XTerm*background: black
XTerm*foreground: green
XTerm*scrollbar: yes
```

在.xinitrc中设置加载该文件

    xrdb -load /home/padicao/.Xresources

于是，既设置了字体，也给出了比较好的配色方案。

### 2, xterm的复制粘贴

在xterm中粘贴：鼠标中键

在xterm中复制：鼠标左键选择即可

粘贴xterm中复制的内容到别的应用：鼠标中键，例如在xterm中左键选择，在firefox中中键粘贴。



