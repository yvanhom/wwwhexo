---
uuid:             fd6eda4c-6394-43c2-91af-465bed0b53ae
layout:           post
title:            'FreeBSD下bash的编程实例 -- 加密解密'
slug:             freebsd-bash-example
subtitle:         null
date:             '2014-11-20T06:45:38.000Z'
updated:          '2015-07-29T22:42:22.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - freebsd
  - shell

---



## 1, 需求说明

对文件的加密和解密，可以使用openssl工具还实现。这个脚本就是简化对这个工具的使用，脚本名称myencrypt.sh，用法如下：

    Usage: myencrypt.sh [-o outfile] [-x] infile

其中：

- -o outfile 指定输出文件的名称，如果不指定，则由脚本生成，生成时 - 如果是加密，那么输出文件格式为：输入文件.des3
- 如果是解密，如果输入文件为 filename.des3，那么输出文件为filename，否则输出文件格式为：输入文件.raw

```
<li>-x 选项判断是加密还是解密，不含该选项则是加密</li>
<li>密码在运行过程中提示输入</li>
```

这里就涉及到shell的以下知识：

- 字符串的简单解析，以生成文件名
- 程序参数的解析，这里使用getopt
- 读取密码，且不回显


## 2, 使用getopt解析参数

以man getopt中的例子做为模板

```
# 参数格式为xo:，表示两个选项，x选项没参数，o选项带参数
args=`getopt xo: $*`
# 这里 -x -o 1.txt filename 会解析成 -x -o 1.txt -- filename
# 不支持将选项参数放后边，比如 -x filename -o 1.txt 会解析成 -x -- filename -o 1.txt
 
# 如果解析出问题，那么提示用法后退出
if [ $? -ne 0 ]; then
    echo 'Usage: myencrypt [-x] [-o outfile] infile"'
    exit 2
fi
# 这个的作用不明，从man getopt中照抄过来的
set -- $args
 
DFLAGS=0
 
while true; do
    case "$1" in
    -x)
        DFLAGS=1
        # 使参数左移一位
        shift
        ;;
    -o)
        outfile=$2
        # 有两个参数，所以需要移动两位
        shift;shift
        ;;
    --)
        # 解析完成，退出
        shift; break
        ;;
    esac
done
 
# 解析后剩下的便是输入文件路径，这里只支持一个文件路径
if [ "$#" -ne 1 ]; then
    echo "You should put one and just one file"
    exit 2
fi
 
infile=$1
```

###  3, 自动生成输出文件名

```
# 如果输入文件不存在，那么便提示退出
if [ ! -f "$infile" ]; then
    echo $infile not found
    exit 1
fi
 
# 如果用户没有指定输出文件名，则自动生成
if [ -z "$outfile" ]; then
    if [ "$DFLAGS" -eq 0 ]; then
        outfile=${infile}.des3
    else
        # 这个的意思是去除掉匹配到的字符串及右边所有的字符，没有匹配到，则是原来的字符串
        outfile=${infile%.des3}
        if [ $outfile == $infile ]; then
            outfile=${infile}.raw
        fi
    fi  
fi
```

更多的字符串操作说明，点击[这里](http://www.cnblogs.com/chengmo/archive/2010/10/02/1841355.html "Shell字符串操作")。

### 3, 读取密码，不回显

```
echo -n "Enter password: "
# 关闭输入回显
stty -echo
read passwd
# 开启输入回显
stty echo
# 重开一行
echo ""
```

### 4, 加密解密

```
# 比较简单，都是使用des3算法做加密解密，-d选项表示解密
if [ $DFLAGS -eq 0 ]; then
    cat $infile | openssl des3 -salt -k $passwd > $outfile
else
    cat $infile | openssl des3 -d -k $passwd > $outfile
```

 



