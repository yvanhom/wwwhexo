---
uuid:             08cb72ec-1916-4e54-948a-98a27a0f45de
layout:           post
title:            openvswitch连接带宽限制
slug:             openvswitch-bandwidth-limit
subtitle:         null
date:             '2015-01-16T06:24:47.000Z'
updated:          '2015-07-29T22:39:23.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - sdx

---


这方面网上的资料不少，但是详细的太详细，太复杂，太长，简短的太简短压根不解释什么，而且不少都只是讲怎么限制，不讲怎么取消限制。

这导致我在这个上折腾了半天，才摸出门路。

网络拓扑如下：

```
-------------------------------------
|        br0 (10.10.10.10)          |
-------------------------------------
|vnet0     |vnet1    |vnet2    |vnet3
|          |         |         |
vm01      vm02      vm03      vm04
(10.10.10.101)  (10.10.10.103)
      (10.10.10.102)     (10.10.10.104)
```

使用openvswitch-2.1.2，控制器使用floodlight-0.9.0，操作系统使用CentOS 7。

### 入口：接口限制

端口的入口限制，比较简单，通过设置端口的接口便可以。如下所示，限制入口100Mbites/s，在10Mbits/s范围内波动（怎么波动，详细的我也不懂）：

```
# 限制入口带宽，单位Kbit/s
ovs-vsctl set interface vnet0 ingress_policing_rate=100000
# 限制波动范围，单位Kbit/s
ovs-vsctl set interface vnet0 ingress_policing_burst=10000
```

限制后，使用iperf测试网络速度，在vm04启动iperf服务器(iperf -s)，在vm01，vm02，vm03同时启动iperf客户端（iperf -c vm04 -t 10，一开始连接不上，简单粗暴地 iptables -F 关闭防火墙试试），在vm04观察到以下结果：

```
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 10.10.10.104 port 5001 connected with 10.10.10.103 port 45863
[  5] local 10.10.10.104 port 5001 connected with 10.10.10.102 port 52650
[  6] local 10.10.10.104 port 5001 connected with 10.10.10.101 port 40958
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-10.0 sec   127 MBytes   106 Mbits/sec
[  5]  0.0-10.2 sec   128 MBytes   106 Mbits/sec
[  6]  0.0-10.0 sec   126 MBytes   106 Mbits/sec
```

可以看到，三个客户端都能达到100Mbits/s左右的速度，表明入口限制成功，出口没有限制。

取消设置也很简单，将上边的两个值都设置为0便可以。

### 出口：QoS限制

出口的限制并不能简单地设置接口，没有egress_policing_rate和egress_policing_burst的设置选项，只能使用QoS进行设置。

一般一个端口有一个接口，有一个qos设置，这个qos设置可以有多个队列，所以，出口的QoS队列便是在端口上创建一个qos设置，在这个qos设置上创建一个qos队列，如下：

```
ovs-vsctl set port vnet0 qos=@newqos -- 
     --id=@newqos create qos type=linux-htb queues=0=@q0 
          other-config:max-rate=100000000 -- 
     --id=@q0 create queue other-config:min-rate=100000000  
          other-config:max-rate=100000000
```

为vnet0创建一个qos设置newqos，拥有一个队列q0，newqos设置最大100Mbits/s，qos队列q0设置最小最小都是100Mbits/s。

网上很多的设置都没有设置newqos的最大速度限制，在我的实验平台上，没有设置这个，那么会自动限制为10Mbits/s，于是q0设置的100Mbits/s就完全失效了，所以还是加上为妙。

取消限制比较复杂，需要先将端口的qos清空，再清理qos和队列。

```
# 清空端口的qos设置
ovs-vsctl clear port vnet0 qos
 
# 清理qos，这里全部清空
ovs-vsctl --all destroy qos
# 若只清空一个，使用下述命令，不过需要先获取_uuid
ovs-vsctl destroy qos '_uuid'
 
# 清理queue，这里全部清空
ovs-vsctl --all destroy queue
# 若只清空一个，使用下述命令，不过需要先获取_uuid
ovs-vsctl destroy queue '_uuid'
```

限制后，使用iperf测试网络速度，在vm04启动iperf服务器(iperf -s)，在vm01，vm02，vm03同时启动iperf客户端（iperf -c vm04 -t 10），跟上边的测试方法一样，在vm04观察到以下结果：

```
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 10.10.10.104 port 5001 connected with 10.10.10.101 port 40959
[  5] local 10.10.10.104 port 5001 connected with 10.10.10.102 port 52651
[  6] local 10.10.10.104 port 5001 connected with 10.10.10.103 port 45864
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-10.3 sec  91.5 MBytes  74.5 Mbits/sec
[  5]  0.0-10.2 sec  20.0 MBytes  16.4 Mbits/sec
[  6]  0.0-10.0 sec  14.8 MBytes  12.4 Mbits/sec
```

可以看到，三个同接收端的客户端已经在相互影响，共享了仅有的100Mbits/s的网络资源。这表明出口限制已经起作用。

### 显示信息

上边已经说过，一个端口有一个接口，一个qos设置，使用 ovs-vsctl list port 会显示所有的端口信息，这里只摘取vnet0的端口信息，可以看到vnet0使用的接口和qos设置：

```
_uuid               : 0981861a-944d-482c-82b0-bc509079fbe1
bond_downdelay      : 0
bond_fake_iface     : false
bond_mode           : []
bond_updelay        : 0
external_ids        : {}
fake_bridge         : false
interfaces          : [b162a1a6-64e5-4878-96fb-43ec0142b38a]
lacp                : []
mac                 : []
name                : "vnet0"
other_config        : {}
qos                 : fce74a51-a57c-496a-a2f0-339675b1d8a2
statistics          : {}
status              : {}
tag                 : []
trunks              : []
vlan_mode           : []
```

使用 ovs-vsctl list interface 列出所有的接口设置，这里也只摘取vnet0的接口设置，可以看到设置的ingress*policing*burst和ingress*policing*rate，本来我还想看看能不能设置link_speed，结果无效，貌似这个值是常量：

```
_uuid               : b162a1a6-64e5-4878-96fb-43ec0142b38a
admin_state         : up
bfd                 : {}
bfd_status          : {}
cfm_fault           : []
cfm_fault_status    : []
cfm_flap_count      : []
cfm_health          : []
cfm_mpid            : []
cfm_remote_mpids    : []
cfm_remote_opstate  : []
duplex              : full
external_ids        : {attached-mac="52:54:00:51:40:9d", iface-id="abdc548d-8d0c-41a6-a89c-496ed6523794", iface-status=active, vm-id="f7185d81-24ac-43a8-b572-ef337fcd811f"}
ifindex             : 10
ingress_policing_burst: 10000
ingress_policing_rate: 100000
lacp_current        : []
link_resets         : 1
link_speed          : 10000000
link_state          : up
mac                 : []
mac_in_use          : "fe:54:00:51:40:9d"
mtu                 : 1500
name                : "vnet0"
ofport              : 5
ofport_request      : []
options             : {}
other_config        : {}
statistics          : {collisions=0, rx_bytes=363024801, rx_crc_err=0, rx_dropped=0, rx_errors=0, rx_frame_err=0, rx_over_err=0, rx_packets=29205, tx_bytes=1940644, tx_dropped=0, tx_errors=0, tx_packets=27492}
status              : {driver_name=tun, driver_version="1.6", firmware_version=""}
type                : ""
```

使用 ovs-vsctl list qos 列出所有的qos设置，这里只摘取vnet0的qos设置，可以看到设置的最大带宽限制，以及qos队列：

```
_uuid               : fce74a51-a57c-496a-a2f0-339675b1d8a2
external_ids        : {}
other_config        : {max-rate="100000000"}
queues              : {0=9e2c7e5f-76bb-4d57-ad51-ffb3ef34df8d}
type                : linux-htb
```

使用ovs-vsctl list queue 列出所有的qos queue设置，这里只摘取vnet0的qos里的queue设置，可以看到最大和最小设置：

```
_uuid               : 9e2c7e5f-76bb-4d57-ad51-ffb3ef34df8d
dscp                : []
external_ids        : {}
other_config        : {max-rate="100000000", min-rate="100000000"}
```

### 平台脚本

在实验平台上，一个端口一个端口地设置会很麻烦，所以写了两个很简单的脚本，limit.sh用于开启限制，unlimit.sh用于取消限制。

两个脚本都使用都vms.cfg：

```
HOSTS="vm01 vm02 vm03 vm04"
VNETS="vnet0 vnet1 vnet2 vnet3"
```

limit.sh如下：

```
#!/bin/bash
source vms.cfg
 
limit=100  # Mbits/s
 
for v in $VNETS; do
    ovs-vsctl set interface $v ingress_policing_rate=$((limit*1000))
    ovs-vsctl set interface $v ingress_policing_burst=$((limit*100))
    ovs-vsctl set port $v qos=@newqos -- 
             --id=@newqos create qos type=linux-htb queues=0=@q0 other-config:max-rate=$((limit*1000000)) -- 
             --id=@q0 create queue other-config:min-rate=$((limit*1000000)) other-config:max-rate=$((limit*1000000))
done
```

unlimit.sh如下：

```
#!/bin/bash
source vms.cfg
 
for v in $VNETS; do
    ovs-vsctl set interface $v ingress_policing_rate=0
    ovs-vsctl set interface $v ingress_policing_burst=0
    ovs-vsctl clear Port $v qos
done
ovs-vsctl --all destroy qos
ovs-vsctl --all destroy queue
```
 



