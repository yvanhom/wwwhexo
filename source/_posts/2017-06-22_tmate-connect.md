---
uuid:             c11b4297-3e1a-4e08-aacc-76b189c613fe
layout:           post
title:            '通过 tmate 连接内网机器'
slug:             tmate-connect
subtitle:         null
date:             '2017-06-22T11:08:57.000Z'
updated:          '2017-08-25T05:01:56.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux
  - shell

---

## tmate

tmate: Instant terminal sharing

tmate 是即时终端会话分享工具，不过我们可以通过分享会话，使得操作内网机器变得可能。

### 安装使用

安装很简单，可以直接[下载](https://github.com/tmate-io/tmate/releases)已经编译好的二进制文件，直接运行：

```
tmate
```
会启动一个新的会话，连接 tmate.io，将新会话分享给 tmate.io，返回一个 tmate.io 的 ssh 链接，比如 `1750e36b84d5bbe50@tmate.io`。

经此，其他机器便可以通过 tmate.io 给的链接操作该内网机器：

```
ssh 1750e36b84d5bbe50@tmate.io
```

在会话中，使用 `exit` 可以退出会话，断开跟 tmate.io 的连接。

如果只是想断开会话的连接，而不想退出会话，则需要使用快捷键 `CTRL+B D`（按住 CTRL 和 D，然后都松开，再按 D）。

### 自建 tmate 服务器

如果不想使用 tmate.io 的服务器，可以自建 tmate 服务器。

根据官方提供的脚本，运行，解决依赖问题，构建：

```
git clone https://github.com/tmate-io/tmate-slave.git
cd tmate-slave
./create_keys.sh # 记住这里产生的 footprints
./autogen.sh
./configure
make
```

运行（指定绑定的主机和端口，`./create_keys.sh` 生成的 keys 目录）：

```
tmate-slave -k ./keys -p 34564 -h 0.0.0.0
```

客户端需要配置 ~/.tmate.conf 以使用新的服务器，同样的需要指定主机，端口，生成的 footprints 等：

```
set -g tmate-server-host <HOST>
set -g tmate-server-port 34564
set -g tmate-server-rsa-fingerprint   "xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx"
set -g tmate-server-ecdsa-fingerprint "xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx"
set -g tmate-identity ""
```
