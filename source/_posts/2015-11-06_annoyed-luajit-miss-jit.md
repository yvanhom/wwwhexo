---
uuid:             39c8921c-f57c-487f-ba79-7fdd7ba7a11e
layout:           post
title:            '恼火的 luajit 编译二进制码'
slug:             annoyed-luajit-miss-jit
subtitle:         null
date:             '2015-11-06T04:04:45.000Z'
updated:          '2015-11-06T04:04:45.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - lua

---

Windows + msys 下，使用 mingw64/mingw-w64-x86_64-luajit-git 的 luajit，运行下述命令，将 lua 的源码文件编译成二进制码：

```sh
luajit -b test.lua test2.lua
```

test.lua 内容如下：

```lua
print("Hello, World!")
```

运行失败，报错误：

```
msys64\mingw64\bin\luajit.exe: unknown luaJIT command or jit.* modules not installed
```

原因就是 jit 目录不对，luajit 的 jit 目录在 `/mingw64/share/luajit-2.0.4/jit/`，直接复制到当前目录：

```sh
cp -r /mingw64/share/luajit-2.0.4/jit/ ./
```

终于编译成功，可以像使用源代码一样，直接运行编译后的二进制码：

```sh
luajit test2.lua
```

总之在哪运行 luajit 进行编译就需要在哪复制一份 jit，真是麻烦得让人恼火。

