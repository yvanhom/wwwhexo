---
uuid:             02bfcca4-8c00-44ed-8549-fea3ebc91293
layout:           post
title:            openvswitch连接带宽限制（补充）
slug:             openswitch-bandwidth-limit-2
subtitle:         null
date:             '2015-01-20T22:37:42.000Z'
updated:          '2015-12-09T08:37:08.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - sdx

---


对[上一篇文章](/2015/01/16/openvswitch-bandwidth-limit/ "openvswitch限制连接带宽")的补充。

### 1, 虚拟交换机间的连接限制

openvswitch默认在br间使用patch port来连接，方法如下：

```
# 添加端口
ovs-vsctl add-port br0 p0-0
ovs-vsctl add-port br1 p1-0
 
# 将端口设置为patch模式，并一对一匹配
ovs-vsctl set interface p0-0 type=patch
ovs-vsctl set interface p0-0 options:peer=p1-0
ovs-vsctl set interface p1-0 type=patch
ovs-vsctl set interface p1-0 options:peer=p0-0
```

不过这种方法不能限制连接的速度，google了一番，发现可以使用veth来构建，veth会提供一对匹配的端口，从一端入，便从另一端出，方法如下：

```
# 创建一对veth端口，名称是veth0和veth1
ip link add name veth0 type veth peer name veth1
 
# 将接口加入br中，让br0跟br1相连
ovs-vsctl add-port br0 veth0
ovs-vsctl add-port br1 veth1
```

限制速度也简单，使用openvswitch的入口限制：

```
ovs-vsctl set interface veth0 ingress_policing_rate=100000 ovs-vsctl set interface veth0 ingress_policing_burst=10000 ovs-vsctl set interface veth1 ingress_policing_rate=100000 ovs-vsctl set interface veth1 ingress_policing_burst=10000
```

发送端不限制，速度快，接收端限制，速度慢，貌似接收端会频繁丢包，这点不如发送端速度慢，接收端游刃有余，于是整整发送端的流控。

### 2, 连接host端的端口出口限制

openvswitch使用的linux-htb来进行出口流量控制，在我的测试平台下，使用linux-htb，限制vm1到vm2中的某端链路，有以下测试结果：

```
vm1 ==》 vm3：
    96.7 Mbits/sec
vm1 ==》 vm3，vm3 ==》 vm1：
    34.0 Mbits/sec
    95.6 Mbits/sec
```

可以看到，双向连接时，上行严重影响下行，不理想。

于是尝试使用tc的tbf进行速度限制，命令如下：

```
# 网上很多的设置都没有mpu，mtu，在我的平台下，没有mtu的设置只有1mbit的速度，
# tbf更适用于低速度的限制，一般是1mbit/s以下，默认mtu也是1mbit/s的设置 
tc qdisc add dev vnet0 root tbf rate 100mbit latency 200ms burst 100000 mpu 64 mtu 150000
tc qdisc add dev vnet0 root tbf rate 100mbit latency 200ms burst 100000 mpu 64 mtu 150000
 
# 取消限制，使用tc qdisc del dev vnet0 root
# 修改参数，使用tc qdisc change dev vnet0 root ......
```

测试结果：

```
vm1 ==》 vm3：
    96.7 Mbits/sec
vm1 ==》 vm3，vm3 ==》 vm1：
    92.8 Mbits/sec
    94.3 Mbits/sec
```

可以看到tbf限制时，上行跟下行的影响非常的小。

看Linux的man page，似乎red更加适用于高速度的限制，使用命令如下：

```
tc qdisc add dev vnet0 root red limit 400000 min 30000 max 90000 avpkt 1000 burst 55 ecn adaptive bandwidth 100Mbit
```

不过在我的实验平台下没起任何效果。

设置后，可以使用以下命令查看当前的设置结果：

    tc qdisc show dev vnet0

下边是使用tbf设置后的显示结果：

    qdisc tbf 8031: root refcnt 2 rate 100000Kbit burst 100000b lat 200.0ms

得出结论，使用tbf进行限速是当前最好的选择，因为htb限速后上行下行影响严重，red限速不知原因地不起效果，有时间再找找不起效果的原因。



