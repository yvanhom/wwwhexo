---
uuid:             23b17975-aae3-49f6-8048-00b32dd7f608
layout:           post
title:            使用rpmbuild生成rpm包
slug:             rpmbuild-build
subtitle:         null
date:             '2014-10-16T23:32:38.000Z'
updated:          '2015-07-29T22:43:27.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


在[上一篇文章](https://www.yvanhom.com/archives/120 "AutoTools流程速记")中，已经能够生成tar.gz的源码包，这里介绍如何生成rpm包，也是一个流程速记


## 1, 编写spec文件

模版如下（不得不说vim真心好用，直接vim xyw-pi.spec，直接出现一个模版，直接填空就行了，需要把不需要填的注释掉，否则会出错）：

```
#
# spec file for package xyw-pi
#
# Copyright (c) 2014 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
 
# Please submit bugfixes or comments via http://bugs.opensuse.org/
#
 
Name:           xyw-pi
Version:        1.0
Release:        1
License:        GPL-3.0
Summary:        A program to calculate pi
Url:            https://www.yvanhom.com
Group:          Productivity/Other
Source:         xyw-pi-1.0.tar.gz
#Patch:
BuildRequires:  gcc
#PreReq:
#Provides:
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
 
%description
A program to calculate pi.
Use the random method. It generates many points, count those in the 1/4 pie,
and calculate the pi.
 
%prep
%setup -q
 
%build
%configure
make %{?_smp_mflags}
 
%install
make install DESTDIR=%{buildroot} %{?_smp_mflags}
# Write a proper %%files section and remove these two commands and
# the '-f filelist' option to %%files
echo '%%defattr(-,root,root)' >filelist
find %buildroot -type f -printf '/%%P*n' >>filelist
 
 
%clean
rm -rf %buildroot
 
%post
 
%postun
 
%files -f filelist
%defattr(-,root,root)
%doc ChangeLog README COPYING
```


## 2, 将文件放在适当的位置

```
cp xyw-pi-1.0.tar.gz /home/padicao/rpmbuild/SOURCES/
cp xyw-pi.spec /home/padicao/rpmbuild/SPECS/
```


## 3, 生成rpm包

- rpm -bb spec路径 生成rpm包
- rpm -bs spec路径 生成src.rpm包，只是打包了tar.gz和spec文件
- 加–clean，在生成后进行清理



