---
uuid:             5f74a20e-94b0-4816-8e36-4e02a55bf38d
layout:           post
title:            'SSH 端口转发补充'
slug:             ssh-port-redirection-2
subtitle:         null
date:             '2015-04-16T23:46:04.000Z'
updated:          '2015-12-09T20:49:21.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


上一篇文章见[链接](/2014/11/12/ssh-port-redirection/ "SSH端口转发")，这里作一些补充。

### X 协议转发

SSH 的 X 协议转发使用 -X 选项打开。

最近使用时，总是遇到以下问题：

```
Warning: untrusted X11 forwarding setup failed: xauth key data not generated Warning: No xauth data; using fake authentication data for X11 forwarding.
```

打开 firefox 提示

```
Invalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyError: cannot open display: localhost:10.0
```

这时候可以使用 -Y 选项，该选项表示使用加密通道。

### SSH 代理

设置 SSH 代理很简单，便是使用 SSH 的动态端口转发，这里使用 OpenShift 做下实验。

运行：

```
# rh_blog 是我在 ~/.ssh/config 中设置的 OpenShift 主机 
ssh -qTfnN -D 35678 rh_blog
```

然后在 foxyproxy 中设置使用 127.0.0.1:35678，**勾中 Socks 代理**，如此，便可以使用该代理绕过一些墙。

PS: 从今天开始，混在中文中的英文将用空格保护起来。



