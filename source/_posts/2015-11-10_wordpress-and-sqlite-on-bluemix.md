---
uuid:             72c28ee6-5107-4965-9ad1-37137fe45c81
layout:           post
title:            'Bluemix 上部署 WordPress + SQLite'
slug:             wordpress-and-sqlite-on-bluemix
subtitle:         null
date:             '2015-11-10T21:16:13.000Z'
updated:          '2015-11-12T20:00:20.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - build-site

---

IBM 的 [Bluemix](https://console.ng.bluemix.net/home/) 免费套餐很良心，365GB 小时的免费额度，可以支持每月 512MB 不间断使用。可以免费使用的包括 Cloud Foundry 和 Container，前者是 Paas，可以使用 PHP，Python，NodeJS 等，后者是 Docker，可以像虚拟机一样操作。

这里介绍的是使用 Cloud Foundry 的 PHP 环境部署基于 SQLite 的 WordPress。不使用 MySQL 是因为 Bluemix 的 MySQL 免费额度太坑，只有 5M 大小，仅支持 2 个并发连接。

### 新建 PHP 应用

在 仪表板 -- 创建应用程序 -- Web -- PHP -- 继续 -- 输入名称 -- 完成 于是便有了一个 PHP 应用。

回到 仪表板，点击刚刚创建的 PHP 应用，点击 Add git，根据提示完成 git 的设置。

参考 [freehao123/ibm-blumix](http://www.freehao123.com/ibm-bluemix/)。

### 克隆到本地

```sh
git clone https://hub.jazz.net/git/***/****
```

貌似只支持 https，每次都需要输入用户名和密码。

下载 [WordPress](https://wordpress.org/download/)，解压到克隆的根目录。

### 安装 WordPress 并配置

下载 [SQLite Integration](https://wordpress.org/plugins/sqlite-integration/)，解压到 `wp-content/plugins`。

复制 db.php 到 wp-content：

```sh
cp wp-content/plugins/sqlite-integration/db.php wp-content/
```

复制 wp-config-sample.php 为 wp-config.php，并修改：

```php
// 注意不能缺少分号
// 1. 禁用 MySQL
define('USE_MYSQL', false);
// 2. 访问 https://api.wordpress.org/secret-key/1.1/salt/ 并将结果替换掉
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');
```

关于如何在 WordPress 下使用 SQLite，可以参考 [SQLite Integration/Installation](https://wordpress.org/plugins/sqlite-integration/installation/)。

### 设置 PHP

Bluemix 点赞的地方便是开放 PHP，可以自由地选择 PHP 版本，自由地增加模块。

为运行 WordPress 和 MySQL，需要为 PHP 增加一些模块。

在克隆后的根目录下新建目录 .bp-config，在该目录下新建 options.json 文件，内容如下：

```json
{
    "PHP_EXTENSIONS": ["openssl", "pdo", "pdo_sqlite", "sockets", "gd", "curl", "zip", "mbstring", "mysqli", "mcrypt"]
}
```

参考 [cf-ex-wordpress](https://github.com/dmikusa-pivotal/cf-ex-wordpress/blob/master/.bp-config/options.json) 并增加了两个 SQLite Integration 的依赖 pdo 和 pdo_sqlite。

### 推送更新

使用 git 运行：

```sh
git add .
git commit -m 'Install WordPress, sqlite and setup PHP'
git push
```

在仪表板重新启动应用程序。

进网站后便可以进入 WordPress 的初始设置界面了。

### 总结

完全不需要使用 cf 工具，只需要使用 git 和 IBM Bluemix 的仪表板便完成了部署。

部署时我遇到的最大问题便是提示缺少 PDO 和 PDO SQLITE，这个需要配置 PHP，启动这些模块。

其次是没有响应，最后发现是 wp-config.php 增加语句时忘了打分号，白白浪费了不少时间。

