---
uuid:             99de49bf-0832-4177-bf9e-edbcaa4c3e0a
layout:           post
title:            '使用openSUSE build service来生成rpm'
slug:             opensuse-build-service
subtitle:         null
date:             '2014-10-16T23:52:37.000Z'
updated:          '2015-07-29T22:43:17.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - linux

---


参考[这个](https://lug.ustc.edu.cn/sites/opensuse-guide/obs.php "软件打包服务")，这个真心详细，好用，这里把一些命令简单归纳一下：

- osc checkout ***projectname ***将项目下载到本地，projectname格式一般为home:username:subproj
- osc meta prj -e ***projectname*** 编辑项目元数据，可以在这里添加为什么系统生成rpm

```
<project name="home:padicao:hello">
  <title>xyw-pi</title>
  <description>A program to calculate pi</description>
  <person userid="padicao" role="maintainer"/>
 
  <repository name="openSUSE_12.3">
    <path project="openSUSE:12.3" repository="standard"/>
    <arch>i586</arch>
    <arch>x86_64</arch>
  </repository> 
</project>
```

- osc up ***projectname******* 本地更新
- 把tar.gz和spec文件放入包目录下，目录一般为***projectname/packagename***

进入包目录

- osc vc 编辑，记录想要做的修改
- osc add * 将新增文件加入
- osc commit编辑提交记录，并提交
- osc log查看提交记录
- osc results 查看打包结果。提交后，openSUSE build service会调度进行构建，并打包，运行这个查看打包进度
- osc buildlog openSUSE_12.3 x86_64 查看打包记录，如果打包失败，可以看看失败原因，打包成功，也可以看看有哪些警告



