---
uuid:             698b3b15-f192-455b-9787-98c9edc37943
layout:           post
title:            '使用 trap 处理 shell 的 Ctrl+C 信号'
slug:             trap-interrupt-in-shell
subtitle:         null
date:             '2015-11-26T22:50:45.000Z'
updated:          '2015-11-26T22:51:35.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags:
  - shell

---

Linux 下 shell 脚本的使用非常方便，可以将一大堆繁琐的命令整合到一起，一键执行。

我这里遇到的问题是，使用脚本启动了一堆监控命令，很多都在后台运行，监控结束时，手动使用 `Ctrl+C` 结束。可是结束时，只有前台的命令被结束了，后台的命令还在执行。

怎么办？再写一个脚本结束那些后台的监控命令？这个太麻烦了。更好的方法是通过脚本获取监控结束的时机而不是手动结束。这里采取中策，那就是在脚本中使用 trap 处理 `Ctrl+C` 信号。

trap 的使用很简单，如下：

```sh
trap '{ echo "Ctrl+C"; exit; }' INT
```

脚本中加入这一行，会使得 `Ctrl+C` 触发时，执行 trap 命令中的语句。上边的语句会打印 'Ctrl+C'，然后退出。

需要注意的是

* '{ }' 大括号之间的空格不能省略，语句后的 ';' 分号不能省略
* 如果没有 exit，那么程序不会退出

回到我的例子，将代码精简了不少，如下：

```sh
# 关闭相关后台
function myexit() {
        echo 'Ctrl + C'
        ssh host1 pkill iostat
        ssh host2 pkill iostat
        exit
}

trap "{ myexit; }" INT

-- 启动后台监控
ssh host1 iostat -x -d /dev/sda 5 > 1.log &
ssh host2 iostat -x -d /dev/sdb 5 > 2.log &

-- 前台监控
while true; do
        grep Mem /proc/meminfo >> 3.log
        sleep 5
done
```
