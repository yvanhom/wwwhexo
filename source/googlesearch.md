---
uuid:             350b6b08-c4f1-4e05-9193-80cfcb0c80c0
layout:           post
title:            'Search www.yvanhom.com'
slug:             googlesearch
subtitle:         null
date:             '2015-11-11T03:26:35.000Z'
updated:          '2016-04-11T22:15:31.000Z'
author:           'Yvan Hom'
author_slug:      yvan
header_img:       null
status:           published
language:         en_US
meta_title:       null
meta_description: null
tags: null

---

<form method="get" action="https://www.google.com/search">
    <input type="text" placeholder="Search..." name="q" value="" /> 
    <input type="hidden" name="sitesearch" value="www.yvanhom.com" />
</form>
